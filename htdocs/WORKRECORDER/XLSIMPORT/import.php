<?php
        //**************************************************************
        //**  THIS PHP UPLOADS XLS file rows into WORKER Database TABLE
        //**************************************************************
        session_start();
        
        include("PHPExcel/IOFactory.php");
        
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }
        
	//connection to MySQL
	require_once('../db_connect.php');
	
	
        function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
?>

<html>

<!-- HTML INNER CSS-->
<style>
	h1 {
			width: 80%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	
</style>

<head>
    <title> XLS importálása... </title>

</head>

<body>

    <h1> XLS importálása ...</h1>
    
    Korábbi XLS-formátumú "Munkavállalói Adattábla" feltöltése a Dolgozók adatai közé... <br/> <br/>
    
    <form action="import.php" method="post" enctype="multipart/form-data">
        XLS feltöltése :
        <input type="file" name="fileToUpload" id="fileToUpload">
        <input type="submit" value="Feltöltés >" name="submit">
    </form>
    

<?php
    if($_SERVER['REQUEST_METHOD'] == "POST")
    {

        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        
        // Check if file already exists
        if (file_exists($target_file))
        {
            echo "Sajnáljuk, ez már egy létező fájl a rendszerben.";
            $uploadOk = 0;
        }
        
        // Allow certain file formats
        if($imageFileType != "xls" && $imageFileType != "xlsx" && $imageFileType != "XLS" && $imageFileType != "XLSX" )
        {
            echo "Hiba! Csak XLS formátum megengedett!";
            $uploadOk = 0;
        }
        
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<br/> Az új fájl nem került feltöltésre.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "A fájl ". basename( $_FILES["fileToUpload"]["name"]). " feltöltése sikeres.";
            } else {
                echo "Hiba történt a fájl ellenörzése során.";
            }
        }
        
        if ($uploadOk == 1) 
        {
            //INITIALIZE EMPTY VARs
            $generatedNumber = " "; $id_card_number = " "; $name = " "; $tax_number = " "; $taj_number = 0; $status = " "; $home_postcode = 0;
            $home_city = " "; $home_street = " "; $home_house = " "; $home_floordoor = " "; $mothers_name = " "; $birth_place = " ";
            $birth_date = " "; $education = " "; $telephone_number = " "; $email = " "; $startjob = " "; $endjob = " ";
            
            // prepare and bind
            $prepared_statement = $con->prepare("INSERT INTO Worker VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            $prepared_statement->bind_param("ssssisissssssssssss",$generatedNumber,$id_card_number,$name,$tax_number,$taj_number,$status,$home_postcode,$home_city,$home_street,$home_house,$home_floordoor,$mothers_name,$birth_place,$birth_date,$education,$telephone_number,$email,$startjob,$endjob);
            
            $html = "<table border='1'>";
            $html.= "<tr>";
            $html.= "   <td>Név</td>";
            $html.= "   <td>Személyi ig.</td>";
            $html.= "   <td>Adószám</td>";
            $html.= "   <td>TAJ-szám</td>";
            $html.= "   <td>Státusz</td>";
            $html.= "   <td>Ir.szám</td>";
            $html.= "   <td>Város</td>";
            $html.= "   <td>Utca</td>";
            $html.= "   <td>Hsz</td>";
            $html.= "   <td>Em/Aj</td>";
            $html.= "   <td>Anyja neve</td>";
            $html.= "   <td>Szül. hely</td>";
            $html.= "   <td>Szül. idö</td>";
            $html.= "   <td>Végzettség</td>";
            $html.= "   <td>Telefon</td>";
            $html.= "   <td>E-mail</td>";
            $html.= "   <td>Munkav. kezd.</td>";
            $html.= "   <td>Munkav. vége</td>";
            $html.= "</tr>";
            $objPHPExcel = PHPExcel_IOFactory::load($target_file);
            
            foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                for ($row=3; $row<=$highestRow; $row++)
                {
                    $generatedNumber = generateRandomString(15); //generate DB-ROW ID
                    
                    //get DATA FROM CELLS INTO VARs
                    $name = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(0, $row)->getValue());
                    $id_card_number = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $tax_number = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $taj_number = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(3, $row)->getValue());
                    $status = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(4, $row)->getValue());
                    $home_postcode = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(5, $row)->getValue());
                    $home_city = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(6, $row)->getValue());
                    $home_street = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(7, $row)->getValue());
                    $home_house = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(8, $row)->getValue());
                    $home_floordoor = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(9, $row)->getValue());
                    $mothers_name = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(10, $row)->getValue());
                    $birth_place = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(11, $row)->getValue());
                    $birth_date = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(12, $row)->getValue());
                    $education = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(13, $row)->getValue());
                    $telephone_number = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(14, $row)->getValue());
                    $email = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(15, $row)->getValue());
                    $startjob = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(16, $row)->getValue());
                    $endjob = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(17, $row)->getValue());
                        if ($endjob == " ") { $endjob = "NULL";}	// Handling NULL Value
                        
                    //CHECK IF EXCEL ROW ALREADY EXISTS IN DB
                    $checkDBResult = mysqli_query($con, "select Count(Worker.ID_Card_Number) AS DB_RESULT from Worker where Worker.ID_Card_Number='{$id_card_number}'");
                    $checkDBRow = mysqli_fetch_array($checkDBResult);
                    
                    if ($checkDBRow['DB_RESULT'] != 1) //IF NOT IN DB THEN INSERT NEW RECORD
                    {
                        $html.= "<tr>";
                        
                        $html.= "<td>".$name."</td>";
                        $html.= "<td>".$id_card_number."</td>";
                        $html.= "<td>".$tax_number."</td>";
                        $html.= "<td>".$taj_number."</td>";
                        $html.= "<td>".$status."</td>";
                        $html.= "<td>".$home_postcode."</td>";
                        $html.= "<td>".$home_city."</td>";
                        $html.= "<td>".$home_street."</td>";
                        $html.= "<td>".$home_house."</td>";
                        $html.= "<td>".$home_floordoor."</td>";
                        $html.= "<td>".$mothers_name."</td>";
                        $html.= "<td>".$birth_place."</td>";
                        $html.= "<td>".$birth_date."</td>";
                        $html.= "<td>".$education."</td>";
                        $html.= "<td>".$telephone_number."</td>";
                        $html.= "<td>".$email."</td>";
                        $html.= "<td>".$startjob."</td>";
                        $html.= "<td>".$endjob."</td>";
                        
                        $html.= "</tr>";
                        
                        $prepared_statement->execute();
                    }
                }       //END OF CELLS/ ONE ROW
            }       //END OF FOREACH ROW
            
            $html .= "</table>";
            echo $html;
            echo '<br/> Adatok importálva.';
            echo '	<button onclick="location.href= \'../WORKERS/workers.php\'" type="button">';
            echo ' 		Tovább a Nyilvántartási felületre »»»';
            echo '	</button>';
            $prepared_statement->close();
            $con->close();

        }   //END OF Update Success

	
        
    } // END OF POST
    
?>
    
</body>
</html>
