<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>WorkRecorder</title>
<link rel="stylesheet" type="text/css" href="login_style.css" />
</head>
<body>

<div class="container">
	<section id="content">
            <h2>WorkRecorder</h2>
            <h4>Munkavégzés Nyilvántartó v1.0</h4>
		<form action="index.php" method="POST">
			<h1>Bejelentkezés</h1>
			<div>
				<input type="password" placeholder="Adminisztrációs azonosító" name="username" id="username" />
			</div>
			<div>
				<input type="password" placeholder="Jelszó" name="password" id="password" />
			</div>
			
				<input type="submit" value="OK" >
				<fieldset style="text-align: left;margin-left: 170px; border:1px solid #999; border-radius:8px;	box-shadow:0 0 10px #999;">
                                <legend style="background:#fff; margin:0 auto;">(Bejelentkezési Felület)</legend>
				<input type="radio" name="loginToWhere" value="1" checked> Nyilvántartó -> <br/>
				<input type="radio" name="loginToWhere" value="2"> XLS Import... <br/>
				<input type="radio" name="loginToWhere" value="3" disabled> Napló Megtekintése
				</fieldset>

		</form><!-- form -->

	</section><!-- content -->

</div><!-- container -->
	<p align="left">
            Jobb megjelenés minimum 1600x900-as felbontáson.<br/>
            A legjobb megjelenés 1920x1080-as felbontáson biztosított.
        </p>
	<p align="right">
            © Készítette: Katona Tamás, 2017.
        </p>


<?php
    session_start(); //initialize the sessions
    $_SESSION['access'] = 0;

    if($_SERVER['REQUEST_METHOD'] == "POST")
    {

        $allowed_users =        array('username1',  'kuttor',   'katona6',   'bitman',  'vecsey');
        $allowed_passwords =    array('pass1',      'cgxb46',   'o3gz9l',    'bitman',  'es9naa');
    
        if(in_array($_POST['username'], $allowed_users))
        {
            $key = array_search($_POST['username'], $allowed_users);

            if($allowed_passwords[$key]==$_POST['password'])
            {
                if($_POST['loginToWhere'] == 1){
                    $_SESSION['access'] = 1; //if login is successful create a session that can be authenticated 
                    header("Location: ./WORKLIST/worklist.php");
                }
                if($_POST['loginToWhere'] == 2){
                    $_SESSION['access'] = 1; //if login is successful create a session that can be authenticated 
                    header("Location: ./XLSIMPORT/import.php");
                }


            } else //if password is incorrect reload the login form
                {
                    //header("Location: " . $_SERVER['PHP_SELF']);
                    //die ("Password incorrect, error, redirecting to login");
                }


        }
        die ('Nem megfelelő bejelentkezési adatok a <br/> <u>'.$_POST['username'].'</u> adminisztrációs azonosítóval és <u>'.$_POST['password']. '</u>  jelszóval! <br/> Próbálja úrja!'  );
        
    }


?>






</body>
</html>
