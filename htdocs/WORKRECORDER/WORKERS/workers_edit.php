<html>

<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}

	h1 {
			width: 50%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	.infield{
			border-top-left-radius: 30% 30%;
		    border-bottom-right-radius: 20% 50%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	.longinfield{
			border-top-left-radius: 10% 40%;
		    border-bottom-right-radius: 10% 40%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	
</style>


<?php

	//connection to MySQL
	require_once('../db_connect.php');

?>
<head>
<title>Munkás szerk.</title>


</head>
<body>



<?php
        session_start();
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }

/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	echo '<h1> Munkásadatok szerkesztése </h1>';
	
	// BACK TO WORKERS Button - opens previous Page
	echo '<form action="workers.php">';
	echo '    <input type="submit" value="<< Vissza" />';
	echo '</form>';

	$showEditTable = 0 ;	// DISABLE Editor by Default

	//GET Editable fields by ID from Database
	$result = mysqli_query($con,"SELECT * FROM Worker WHERE WorkerID='{$_GET['id']}'");
	$row = mysqli_fetch_array($result);
	if ( $row['Name'] == "") 
	{

		echo 'HIBA TÖRTÉNT <br/> Nincs ilyen Munkás ezzel a Személyi igazolvány számmal, <br/> ezért nincs mit szerkeszteni...';
	}
	if ( $row['Name'] != ""){
		$showEditTable=1 ;
	}


	if ($showEditTable==1){

?>

		<form name="editmemberform" action="workers_edit.php?id=<?php echo $_GET['id'];?>" method="POST">

			<table border="2" bgcolor="a7adc5" align="center">

				<tr>
					<td> Személyi igazolvány száma:</td>
					<td> <INPUT class="infield" type="text" name="ID_Card_Number" value="<?php echo $row['ID_Card_Number'];?>" SIZE="15" >*</td>
				</tr>

				<tr>
					<td> Név:</td>
					<td> <INPUT class="longinfield" type="text" name="Name" value="<?php echo $row['Name']; ?>"   SIZE="30" >*</td>
				</tr>

				<tr>
					<td> Adószám:</td>
					<td> <INPUT class="infield" type="text" name="TAX_Number" value="<?php echo $row['TAX_Number']; ?>" SIZE="15" >*</td>
				</tr>



				<tr>
					<td> TAJ-szám:</td>
					<td> <INPUT class="infield" type="text" name="TAJ_Number" value="<?php echo $row['TAJ_Number']; ?>" SIZE="15" >*</td>
				</tr>

				<tr>
					<td> Státusz:</td>
					<td> <INPUT class="infield" type="text" name="Status" value="<?php echo $row['Status']; ?>" SIZE="15" ></td>
				</tr>
				<tr>
					<td colspan="2" align="center"> ---- LAKHELY ----</td>
					
				</tr>

				<tr>
					<td> Irányítószám:</td>
					<td> <INPUT class="infield" type="text" name="HOME_PostCode" value="<?php echo $row['HOME_PostCode']; ?>" SIZE="6" >*</td>
				</tr>
				<tr>
					<td> Város:</td>
					<td> <INPUT class="longinfield" type="text" name="HOME_City" value="<?php echo $row['HOME_City']; ?>" SIZE="30" >*</td>
				</tr>
				<tr>
					<td> Utca:</td>
					<td> <INPUT class="longinfield" type="text" name="HOME_Street" value="<?php echo $row['HOME_Street']; ?>" SIZE="30" >*</td>
				</tr>
				<tr>
					<td> Házszám:</td>
					<td> <INPUT class="infield" type="text" name="HOME_House" value="<?php echo $row['HOME_House']; ?>" SIZE="6" >*</td>
				</tr>
				<tr>
					<td> Em./Ajtó:</td>
					<td> <INPUT class="infield" type="text" name="HOME_FloorDoor" value="<?php echo $row['HOME_FloorDoor']; ?>" SIZE="6" ></td>
				</tr>
				<tr>
					<td colspan="2" align="center"> ---- SZÜLETÉSI ADATOK ----</td>
					
				</tr>
				<tr>
					<td> Anyja neve:</td>
					<td> <INPUT class="longinfield" type="text" name="Mothers_Name" value="<?php echo $row['Mothers_Name']; ?>" SIZE="30" >*</td>
				</tr>
				<tr>
					<td> Születési hely:</td>
					<td> <INPUT class="longinfield" type="text" name="Birth_Place" value="<?php echo $row['Birth_Place']; ?>" SIZE="30" >*</td>
				</tr>
				<tr>
					<td> Születési dátum:</td>
					<td> <INPUT class="infield" type="date" name="Birth_Date" value="<?php echo $row['Birth_Date']; ?>" SIZE="15" >*</td>
				</tr>
				<tr>
					<td colspan="2" align="center"> ---- EGYÉB- és Elérhetőségi ADATOK ----</td>
					
				</tr>
				<tr>
					<td> Végzettség:</td>
					<td> <INPUT class="longinfield" type="text" name="Education" value="<?php echo $row['Education']; ?>" SIZE="30" ></td>
				</tr>
				<tr>
					<td> Telefonszám:</td>
					<td> <INPUT class="infield" type="text" name="Telephone_Number" value="<?php echo $row['Telephone_Number']; ?>" SIZE="20" ></td>
				</tr>
				<tr>
					<td> E-mail cím:</td>
					<td> <INPUT class="infield" type="text" name="Email_Address" value="<?php echo $row['Email_Address']; ?>" SIZE="20" ></td>
				</tr>
				<tr>
					<td> Munkaviszony kezdőnapja:</td>
					<td> <INPUT class="infield" type="date" name="Start_Of_JOB" value="<?php echo $row['Start_Of_JOB']; ?>" SIZE="10" ></td>
				</tr>
				<tr>
					<td> Munkaviszony vége:</td>
					<td> <INPUT class="infield" type="date" name="End_Of_JOB" value="<?php echo $row['End_Of_JOB']; ?>" SIZE="10" ></td>
				</tr>


				<tr>
					<td align="right"> Módosít ></td>
					<td> <INPUT type="submit" name="kuld" value="OK" ><INPUT type="reset" name="reset" value="Alaphelyzet" >  </td>
				</tr>

			</table>

		</form>

<?php
	} 	
	//End of IF - Contitional showEditTable DISPLAY


/*********************************************
***		EVENT POST		   ***
**********************************************  */


    if($_SERVER['REQUEST_METHOD'] == "POST")
    {

	//connection to MySQL
	require_once('../db_connect.php');


	//GET FIELD VALUES INTO VARs
        $idcardnumber = $_POST['ID_Card_Number'];
        $name = $_POST['Name'];
        $taxnumber = $_POST['TAX_Number'];
        $tajnumber = $_POST['TAJ_Number'];
        $stat = $_POST['Status'];
        $postcode = $_POST['HOME_PostCode'];
        $city = $_POST['HOME_City'];
        $street = $_POST['HOME_Street'];
        $house = $_POST['HOME_House'];
        $floor = $_POST['HOME_FloorDoor'];
        $mothername = $_POST['Mothers_Name'];
        $birthplace = $_POST['Birth_Place'];
        $birthdate = $_POST['Birth_Date'];
        $education = $_POST['Education'];
        $telnumber = $_POST['Telephone_Number'];
        $email = $_POST['Email_Address'];
        $start = $_POST['Start_Of_JOB'];
        $end = $_POST['End_Of_JOB'];
		if ($end == "") { $end = "NULL";}	// Handling NULL Value
	
	//UPDATE DB  ---- BASED ON NULL VALUE Handling
        if ($end == "NULL") {
		mysqli_query($con, "UPDATE Worker SET ID_Card_Number='{$idcardnumber}', Name='{$name}', TAX_Number='{$taxnumber}', TAJ_Number='{$tajnumber}', Status='{$stat}', HOME_PostCode='{$postcode}', HOME_City='{$city}', HOME_Street='{$street}', HOME_House='{$house}', HOME_FloorDoor='{$floor}', Mothers_Name='{$mothername}', Birth_Place='{$birthplace}', Birth_Date='{$birthdate}', Education='{$education}', Telephone_Number='{$telnumber}', Email_Address='{$email}', Start_Of_JOB='{$start}', End_Of_JOB={$end} WHERE WorkerID='{$_GET['id']}';") ; 
	}else{
		mysqli_query($con, "UPDATE Worker SET ID_Card_Number='{$idcardnumber}', Name='{$name}', TAX_Number='{$taxnumber}', TAJ_Number='{$tajnumber}', Status='{$stat}', HOME_PostCode='{$postcode}', HOME_City='{$city}', HOME_Street='{$street}', HOME_House='{$house}', HOME_FloorDoor='{$floor}', Mothers_Name='{$mothername}', Birth_Place='{$birthplace}', Birth_Date='{$birthdate}', Education='{$education}', Telephone_Number='{$telnumber}', Email_Address='{$email}', Start_Of_JOB='{$start}', End_Of_JOB='{$end}' WHERE WorkerID='{$_GET['id']}';") ;
	}
        //AFTER UPDATE - PAGE Redirect to workers.php");
	echo '<script> location.replace("workers.php"); </script>';
    }


?>

</body>
</html>
