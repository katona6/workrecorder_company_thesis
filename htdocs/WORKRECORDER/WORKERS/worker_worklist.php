<?php
        //**************************************************************
        //**  THIS PHP LISTS WORKER WORKS with ALL Details
        //**************************************************************
        session_start();
        
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }
        
	//connection to MySQL
	require_once('../db_connect.php');
	
	$worker_name_result = mysqli_query($con,"SELECT * FROM Worker WHERE WorkerID='{$_GET['optionlist']}';");
	$worker_row = mysqli_fetch_array($worker_name_result);
	
	$result = mysqli_query($con,"SELECT * , Workdetails.WeekDay_WorkTime_6_18 * Workdetails.WeekDay_Hourly_Rate_6_18 AS sum1 , Workdetails.WeekDay_OverWorkTime_18_6 * Workdetails.WeekDay_OverHourlyRate_18_6 AS sum2 , Workdetails.WeekEnd_WorkTime_6_18 * Workdetails.WeekEnd_HourlyRate_6_18 AS sum3 , Workdetails.WeekEnd_OverWorkTime_18_6 * Workdetails.WeekEnd_OverHourlyRate_18_6 AS sum4 , Workdetails.WeekDay_WorkTime_6_18 + Workdetails.WeekDay_OverWorkTime_18_6 + Workdetails.WeekEnd_WorkTime_6_18 + Workdetails.WeekEnd_OverWorkTime_18_6 AS TotalHours , Workdetails.TotalCostOfTask - Workdetails.MaterialCostOfTask AS WorkFee FROM Worker , Workdetails , Worklist , Departments WHERE Worker.WorkerID=Workdetails.WorkerID AND Departments.DepartmentID=Worklist.Department_ID AND Worklist.Work_ID=Workdetails.WorkID AND Worker.WorkerID='{$_GET['optionlist']}' AND Worklist.Date BETWEEN '{$_GET['startDate']}' AND '{$_GET['endDate']}' ORDER BY Date");
?>

<html>

<!-- HTML INNER CSS-->
<style>
	h1 {
			width: 80%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	
</style>

<head>
    <title>  <?php echo $worker_row['Name'];?> - Teljesítési Lap </title>
    <script src="../jquery_321.min.js"></script>
    <script type="text/Javascript" src="../xepOnline.jqPlugin.js"></script>
    
</head>
<body>



<?php
        
        

/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
    echo '<h1>Teljesítési Lap</h1>';
    
    // BACK TO DEPARTMENTS Button - opens previous Page
        echo '	<button onclick="location.href= \'workers.php\'" type="button">';
	echo ' 		<< Vissza';
	echo '	</button>';
	echo '	<button onclick="xepOnline.Formatter.Format(\'print_me\',{pageWidth:\'420mm\', pageHeight:\'297mm\', filename:\''.$worker_row['Name'].date('_Y-m-d').'\', render:\'download\'})" type="button" >   ';
	echo ' 		Exportálás PDF-be...';
	echo '	</button>';

?>
    
    <div id="print_me">
    
    <p align="center"><font size="8"><b>Teljesítési Lap</b></font></p>
    <p align="center"><font size="6"><b><?php echo $worker_row['Name'];?> összesített munkáiról </b></font></p>
    Jegyzőkönyv,  ________<u><?php echo $worker_row['Name'];?></u>________ (dolgozó) munkavégzésével kapcsolatos teljesítési elszámolásról &nbsp<br/>
    Személyi igazolványa száma: <u><?php echo $worker_row['ID_Card_Number'];?></u>   <br/>
    Lakcím: <u><?php echo $worker_row['HOME_PostCode'].' '.$worker_row['HOME_City'].', '.$worker_row['HOME_Street'].' '.$worker_row['HOME_House'].'. '.$worker_row['HOME_FloorDoor'];?></u> <br/>
    Munkaviszony kezdete: <u><?php echo $worker_row['Start_Of_JOB'];?></u> 
    <p align="right">Az elszámolás időszaka: _____<u><?php echo $_GET['startDate'];?></u>_____ -- _____<u><?php echo $_GET['endDate'];?></u>_____ -ig </p>
    
    <table border="1" align="center">

            <tr align="center">
                    <td style="padding: 30px;">Dátum</td>
                    <td>Megrendelés<br/>azonosító</td>
                    <td>Berendezés<br/>azonosító</td>
                    <td>Munka leírása / megnevezése</td>
                    <td>Beosztás</td>
                    <td>Üzem</td>
                    <td bgcolor="#cccccc" style="width: 40px;height: 30px;">H-P<br/>Óra<br/>6-18</td>
                    <td bgcolor="#cccccc">H-P<br/>ÓraDíj<br/>6-18</td>
                    <td bgcolor="#aaaaaa">H-P<br/>Díj<br/>6-18</td>
                    <td bgcolor="#cccccc" style="width: 40px;height: 30px;">H-P<br/>Óra<br/>18-6</td>
                    <td bgcolor="#cccccc">H-P<br/>ÓraDíj<br/>18-6</td>
                    <td bgcolor="#aaaaaa">H-P<br/>Díj<br/>18-6</td>
                    <td bgcolor="#777777" style="width: 70px;height: 30px;">Szo-Vas<br/>Óra<br/>6-18</td>
                    <td bgcolor="#777777" style="width: 70px;height: 30px;">Szo-Vas<br/>ÓraDíj<br/>6-18</td>
                    <td bgcolor="#555555" style="width: 70px;height: 30px;">Szo-Vas<br/>Díj<br/>6-18</td>
                    <td bgcolor="#777777" style="width: 70px;height: 30px;">Szo-Vas<br/>Óra<br/>18-6</td>
                    <td bgcolor="#777777" style="width: 70px;height: 30px;">Szo-Vas<br/>ÓraDíj<br/>18-6</td>
                    <td bgcolor="#555555" style="width: 70px;height: 30px;">Szo-Vas<br/>Díj<br/>18-6</td>
                    <td>ÖsszÓra</td>
                    <td>MunkaDíj</td>
                    <td>Anyag-<br/>Költség</td>
                    <td>Összesen (ktg.)</td>
            </tr>
<?php
        $TOTALTOTALCost_Counter = 0;
        while ($data = mysqli_fetch_array($result))
        {
            $TOTALTOTALCost_Counter = $TOTALTOTALCost_Counter + $data['TotalCostOfTask'];
            echo '<tr align="center">';
            echo '      <td>'.$data['Date'].'</td>';
            echo '      <td>'.$data['Order_Number'].'</td>';
            echo '      <td>'.$data['Equipment_ID'].'</td>';
            echo '      <td>'.$data['Work_Description'].'</td>';
            echo '      <td>'.$data['Working_Rank'].'</td>';
            echo '      <td>'.$data['Department_Name'].'</td>';
            echo '      <td>'.$data['WeekDay_WorkTime_6_18'].'</td>';
            echo '      <td>'.$data['WeekDay_Hourly_Rate_6_18'].'</td>';
            echo '      <td bgcolor="#aaaaaa">'.$data['sum1'].'</td>';
            echo '      <td>'.$data['WeekDay_OverWorkTime_18_6'].'</td>';
            echo '      <td>'.$data['WeekDay_OverHourlyRate_18_6'].'</td>';
            echo '      <td bgcolor="#aaaaaa">'.$data['sum2'].'</td>';
            echo '      <td>'.$data['WeekEnd_WorkTime_6_18'].'</td>';
            echo '      <td>'.$data['WeekEnd_HourlyRate_6_18'].'</td>';
            echo '      <td bgcolor="#555555">'.$data['sum3'].'</td>';
            echo '      <td>'.$data['WeekEnd_OverWorkTime_18_6'].'</td>';
            echo '      <td>'.$data['WeekEnd_OverHourlyRate_18_6'].'</td>';
            echo '      <td bgcolor="#555555">'.$data['sum4'].'</td>';
            echo '      <td>'.$data['TotalHours'].' óra</td>';
            echo '      <td>'.$data['WorkFee'].' Ft</td>';
            echo '      <td>'.$data['MaterialCostOfTask'].' Ft</td>';
            echo '      <td>'.$data['TotalCostOfTask'].' Ft</td>';
            echo '</tr>';
        
        
        }





?>


    </table>
        <p align="right">Mindösszesen: <b><u><?php echo $TOTALTOTALCost_Counter?> Ft .</u></b> </p>
        
        <br/>
        Készült:  ________________________-n, <?php echo date('Y-m-d');?>
        <br/>
        <br/>
        <br/>
        <br/>
        
    <table align="center">
        <tr align="center" >
            <td style="padding: 50px;">________________________________</td>
            <td style="padding: 50px;">________________________________</td>
        </tr>
        <tr align="center">
            <td>Munkavállaló</td>
            <td>Vállalkozás vezetője</td>
        </tr>
    </table>
    
    </div>

</body>
</html>
