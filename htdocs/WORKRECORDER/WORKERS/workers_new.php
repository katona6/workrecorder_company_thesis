<html>

<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}

	h1 {
			width: 50%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	.infield{
			border-top-left-radius: 30% 30%;
		    border-bottom-right-radius: 20% 50%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	.longinfield{
			border-top-left-radius: 10% 40%;
		    border-bottom-right-radius: 10% 40%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	
</style>

<?php

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

?>
<?php

	//connection to MySQL
	require_once('../db_connect.php');

?>
<head>
<title>Új Dolgozó</title>


</head>
<body>



<?php
        session_start();
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }

/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	echo '<h1> Új Dolgozó Felvitele ></h1>';
	$generatedNumber = generateRandomString(15);
	
	// BACK TO WORKERS Button - opens previous Page
	echo '<form action="workers.php">';
	echo '    <input type="submit" value="<< Vissza" />';
	echo '</form>';


?>
<!-- <button onclick="getQR();">GET</button> -->
<script>
	/*function getQR(){
  		var content = clipboardData.getData('Text');
  		document.forms['newmemberform'].elements['TAX_Number'].value = content;
	}*/

</script>
		<form name="newmemberform" action="workers_new.php" method="POST">

			<table border="2" bgcolor="a7adc5" align="center">

				<tr>
					<td> Személyi igazolvány száma:</td>
					<td> <INPUT class="infield" type="text" name="ID_Card_Number" SIZE="15" >*</td>
				</tr>

				<tr>
					<td> Név:</td>
					<td> <INPUT class="longinfield" type="text" name="Name" SIZE="30" >*</td>
				</tr>

				<tr>
					<td> Adószám:</td>
					<td> <INPUT class="infield" type="text" name="TAX_Number" SIZE="15" >*</td>
				</tr>



				<tr>
					<td> TAJ-szám:</td>
					<td> <INPUT class="infield" type="text" name="TAJ_Number" SIZE="15" >*</td>
				</tr>

				<tr>
					<td> Státusz:</td>
					<td> <INPUT class="infield" type="text" name="Status" value="Aktiv Dolgozó" SIZE="15" ></td>
				</tr>
				<tr>
					<td colspan="2" align="center"> ---- LAKHELY ----</td>
					
				</tr>

				<tr>
					<td> Irányítószám:</td>
					<td> <INPUT class="infield" type="text" name="HOME_PostCode" SIZE="6" >*</td>
				</tr>
				<tr>
					<td> Város:</td>
					<td> <INPUT class="longinfield" type="text" name="HOME_City" SIZE="30" >*</td>
				</tr>
				<tr>
					<td> Utca:</td>
					<td> <INPUT class="longinfield" type="text" name="HOME_Street" SIZE="30" >*</td>
				</tr>
				<tr>
					<td> Házszám:</td>
					<td> <INPUT class="infield" type="text" name="HOME_House" SIZE="6" >*</td>
				</tr>
				<tr>
					<td> Em./Ajtó:</td>
					<td> <INPUT class="infield" type="text" name="HOME_FloorDoor" SIZE="6" ></td>
				</tr>
				<tr>
					<td colspan="2" align="center"> ---- SZÜLETÉSI ADATOK ----</td>
					
				</tr>
				<tr>
					<td> Anyja neve:</td>
					<td> <INPUT class="longinfield" type="text" name="Mothers_Name" SIZE="30" >*</td>
				</tr>
				<tr>
					<td> Születési hely:</td>
					<td> <INPUT class="longinfield" type="text" name="Birth_Place" SIZE="30" >*</td>
				</tr>
				<tr>
					<td> Születési dátum:</td>
					<td> <INPUT class="infield" type="date" name="Birth_Date" SIZE="15" >*</td>
				</tr>
				<tr>
					<td colspan="2" align="center"> ---- EGYÉB- és Elérhetőségi ADATOK ----</td>
					
				</tr>
				<tr>
					<td> Végzettség:</td>
					<td> <INPUT class="longinfield" type="text" name="Education" SIZE="30" ></td>
				</tr>
				<tr>
					<td> Telefonszám:</td>
					<td> <INPUT class="infield" type="text" name="Telephone_Number" SIZE="20" ></td>
				</tr>
				<tr>
					<td> E-mail cím:</td>
					<td> <INPUT class="infield" type="text" name="Email_Address" SIZE="20" ></td>
				</tr>
				<tr>
					<td> Munkaviszony kezdőnapja:</td>
					<td> <INPUT class="infield" type="date" name="Start_Of_JOB" value="<?php echo date("Y-m-d");?>"   SIZE="10" ></td>
				</tr>
				<tr>
					<td> Munkaviszony vége:</td>
					<td> <INPUT class="infield" type="date" name="End_Of_JOB" SIZE="10" ></td>
				</tr>


				<tr>
					<td align="right"> Új Munkás ></td>
					<td> <INPUT type="submit" name="kuld" value="OK" ><INPUT type="reset" name="reset" value="Alaphelyzet" >  </td>
				</tr>

			</table>

		</form>

<?php


    if($_SERVER['REQUEST_METHOD'] == "POST")
    {

	//connection to MySQL
	require_once('../db_connect.php');


	//GET FIELD VALUES INTO VARs
        $idcardnumber = $_POST['ID_Card_Number'];
        $name = $_POST['Name'];
        $taxnumber = $_POST['TAX_Number'];
        $tajnumber = $_POST['TAJ_Number'];
        $stat = $_POST['Status'];
        $postcode = $_POST['HOME_PostCode'];
        $city = $_POST['HOME_City'];
        $street = $_POST['HOME_Street'];
        $house = $_POST['HOME_House'];
        $floor = $_POST['HOME_FloorDoor'];
        $mothername = $_POST['Mothers_Name'];
        $birthplace = $_POST['Birth_Place'];
        $birthdate = $_POST['Birth_Date'];
        $education = $_POST['Education'];
        $telnumber = $_POST['Telephone_Number'];
        $email = $_POST['Email_Address'];
        $start = $_POST['Start_Of_JOB'];
        $end = $_POST['End_Of_JOB'];
		if ($end == "") { $end = "NULL";}	// Handling NULL Value

	//UPDATE DB  ---- BASED ON NULL VALUE Handling
        if ($end == "NULL") {        
	        mysqli_query($con, "INSERT INTO Worker VALUES ('$generatedNumber','$idcardnumber','$name','$taxnumber','$tajnumber','$stat','$postcode','$city','$street','$house','$floor','$mothername','$birthplace','$birthdate','$education','$telnumber','$email','$start',$end)") ;
	}else{
	        mysqli_query($con, "INSERT INTO Worker VALUES ('$generatedNumber','$idcardnumber','$name','$taxnumber','$tajnumber','$stat','$postcode','$city','$street','$house','$floor','$mothername','$birthplace','$birthdate','$education','$telnumber','$email','$start','$end')") ;
	}
        //AFTER UPDATE - PAGE Redirect to workers.php");
	echo '<script> location.replace("workers.php"); </script>';
    }


?>

</body>
</html>
