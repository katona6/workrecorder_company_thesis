

<html>
<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	th {
	    background-color: #a5a9a4;
	}

	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}
	h1 {
			width: 50%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	#topbuttorbar, #topworkerqueryBAR {
		margin-bottom: 15px;
		    box-shadow: 10px 10px 20px rgba(0,0,0,.7);
	}
	.home{
		background-color: #d3d2ec;
	}

</style>
<?php

        
	//connection to MySQL
	require_once('../db_connect.php');

	//IF Got Delete_ID - Delete it !
	if ( isset( $_GET['delete_id'] ) )
	{
	 $sql_query="DELETE FROM Worker WHERE WorkerID='" . $_GET['delete_id'] . "'" ;
	 mysqli_query($con, $sql_query);
	 header("Location: workers.php");
	}


echo '<head>';
echo '<title>Munkások</title>';

echo '</head>';

echo '<body>';
        session_start();
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }

/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	//get data from table named WORKER
    	$result = $con->query("SELECT * FROM Worker ORDER BY Name ASC");
	echo '     <h1> Minden Dolgozó adata ';
        echo '		<button onclick="location.href= \'../logout.php\'" type="button">';
	echo ' 			 «Kijelentkezés»';
	echo '		</button>';
        echo '</h1>';

	// TOP BUTTON Green Bar
	echo '<div id="topbuttorbar" style="background-color:#78bf62; height:27px;">';
	echo '		<button onclick="location.href= \'workers_new.php\'    " type="button">';
	echo ' 			 + Új Dolgozó felvitele >>';
	echo '		</button> '  ;
	echo '		<button onclick="location.href= \'../XLSIMPORT/import.php\'    " type="button">';
	echo ' 			 + XLS-fájlból... >>';
	echo '		</button> ║ Lekérdezés ideje: ' . date("Y-m-d") . '&nbsp &nbsp' . date("H:i:s") ;

			//CLOCK
			//IF There is Outer Internet Connection Show TIME - else NOT
			$outerConnection = checkdnsrr('timeanddate.com') ; 
			
			if ($outerConnection == 1) {
				echo '<div style="float:right"> <iframe src="http://free.timeanddate.com/clock/i5wp1938/n3316/tlhu19/fn3/ftbi/bat2/tt0/tb2" frameborder="0" width="276" height="20"></iframe>
			</div>'; 
			} //END OF CLOCK

	echo '		<div style="float:right"> ';
	echo '			<button style="background-color:red" onclick="location.href= \'../WORKLIST/worklist.php\'    " type="button">';
	echo ' 				 <font size="4">«Munkák»</font>';
	echo '			</button>';
	echo '			<button style="background-color:yellow" onclick="location.href= \'../WORKDETAILS/workdetails.php\'    " type="button">';
	echo ' 				 <font size="4">«Munkák Tulajdonságai»</font>';
	echo '			</button>';
	echo '			<button style="background-color:blue" onclick="location.href= \'../DEPARTMENTS/departments.php\'    " type="button">';
	echo ' 				 <font size="4">«Üzemek»</font>';
	echo '			</button>';
	echo '			<button style="background-color:green" disabled onclick="location.href= \'../WORKERS/workers.php\'    " type="button">';
	echo ' 				 <font size="4">«Dolgozók»</font>';
	echo '			</button>';	
	echo '		</div>';


	echo '</div>';

	// TOP Worker Query BAR
	echo '<form style="background-color:#beecb0" name="personWorkQuery" id="topworkerqueryBAR" action="worker_worklist.php">';

?>
		Dolgozó munkalista minden üzemben: <SELECT name="optionlist"> <?php 			
			while ($row = mysqli_fetch_array($result)) 
			{
                                $wid = $row['WorkerID'];
				$ca = $row['ID_Card_Number'];

				$na = $row['Name'];

				echo ' <OPTION value="' . $wid .'" > ' . $na .' &nbsp &nbsp &nbsp [' . $ca . ']  </OPTION>\n';

			} ?> </SELECT> 
<?php		
                if ( isset($_SESSION['startDate'])){
                    echo '<input type="date" name="startDate" value='.$_SESSION['startDate'].'>-tól ';
                }else{
                    echo '<input type="date" name="startDate">-tól ';
                }

		if ( isset($_SESSION['endDate'])){
                    echo '<input type="date" name="endDate" value='.$_SESSION['endDate'].'>-ig ';
		}else{
                    echo '<input type="date" name="endDate">-ig    ';
		}
		
		echo '<input type="submit" value="Listázás >>" />';
	

	echo '</form>';

	//print Table Headers
	echo '<table border="2" bgcolor="#D4D4D4" singleSelect="true" >';
	echo '    <tr>';
	echo '		<th>Név ▼</th>';
	echo '		<th>Személyi_IG<br/>Szám</th>';
	echo '		<th>Adószám</th>';
	echo '		<th>TAJ-szám</th>';
	echo '		<th>Státusz</th>';
	echo '		<th>Ir.szám</th>';
	echo '		<th>Város</th>';
	echo '		<th>Utca</th>';
	echo '		<th>Hsz</th>';
	echo '		<th>Em/Ajt</th>';
	echo '		<th>Anyja neve</th>';
	echo '		<th>Szül. hely</th>';
	echo '		<th>Szül. dátum</th>';
	echo '		<th>Végzettség</th>';
	echo '		<th><font size="6">☎</font> Telefon</th>';
	echo '		<th><font size="6">✉</font> E-mail</th>';
	echo '		<th>Munkav. kezd.</th>';
	echo '		<th>Munkav. vége</th>';
	echo '		<th>SZERK</th>';
	echo '		<th>TÖRÖL</th>';
	echo '   </tr>';

	//print Table Contents
    	$result = $con->query("SELECT * FROM Worker ORDER BY Name ASC");
	while ($row = mysqli_fetch_array($result)) {
	    print "<tr align='center'><td>" . $row['Name'] . "</td>";
	    print "<td>" . $row['ID_Card_Number'] . "</td>";
	    print "<td>" . $row['TAX_Number'] . "</td>";
	    print "<td>" . $row['TAJ_Number'] . "</td>";
	    print "<td>" . $row['Status'] . "</td>";
	    print "<td class='home'>" . $row['HOME_PostCode'] . "</td>";
	    print "<td class='home'>" . $row['HOME_City'] . "</td>";
	    print "<td class='home'>" . $row['HOME_Street'] . "</td>";
	    print "<td class='home'>" . $row['HOME_House'] . "</td>";
	    print "<td class='home'>" . $row['HOME_FloorDoor'] . "</td>";
	    print "<td>" . $row['Mothers_Name'] . "</td>";
	    print "<td>" . $row['Birth_Place'] . "</td>";
	    print "<td>" . $row['Birth_Date'] . "</td>";
	    print "<td>" . $row['Education'] . "</td>";
	    print "<td><a href=tel:" . $row['Telephone_Number'] .">" . $row['Telephone_Number'] . "</a></td>";
	    print "<td><a href=mailto:" . $row['Email_Address'] .">" . $row['Email_Address'] . "</a></td>";
	    print "<td>" . $row['Start_Of_JOB'] . "</td>";
	    print "<td>" . $row['End_Of_JOB'] . "</td>";
	    print '<td><a href="workers_edit.php?id=' . $row["WorkerID"] . '"><font size="15">✎</font></a></td>';
?>
<td>  <a href="workers.php?delete_id=<?php echo $row['WorkerID']; ?>" onclick="return confirm(' <?php echo 'Törölje \n Név: ' . $row['Name'] . '\n Személyi igazolvány szám: [ ' . $row['ID_Card_Number'] . ' ] \n Lakhely: ' . $row['HOME_City'];   ?> -t ?    '); " ><font size="15">✖</font></a>  </td></tr>
<?php
	}

	echo '</table>';

	
	//close Connection
	mysqli_free_result($result);
	mysqli_close($con);



?>
</body>
</html>
