<html>

<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}

	h1 {
			width: 50%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	.infield{
			border-top-left-radius: 30% 30%;
		    border-bottom-right-radius: 20% 50%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	.longinfield{
			border-top-left-radius: 10% 40%;
		    border-bottom-right-radius: 10% 40%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	
</style>


<?php

	//connection to MySQL
	require_once('../db_connect.php');

?>
<head>
<title>Üzemnév szerk.</title>


</head>
<body>



<?php
        session_start();
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }

/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	echo '<h1> Üzemnév szerkesztése </h1>';
	
	// BACK TO DEPARTMENTS Button - opens previous Page
	echo '<form action="departments.php">';
	echo '    <input type="submit" value="<< Vissza" />';
	echo '</form>';

	$showEditTable = 0 ;	// DISABLE Editor by Default

	//GET Editable fields by ID from Database
	$result = mysqli_query($con,"SELECT * FROM Departments WHERE DepartmentID='{$_GET['id']}'");
	$row = mysqli_fetch_array($result);
	if ( $row['Department_Name'] == "") 
	{

		echo 'HIBA TÖRTÉNT <br/> Nincs ilyen Üzemazonosító, <br/> ezért nincs mit szerkeszteni...';
	}
	if ( $row['Department_Name'] != ""){
		$showEditTable=1 ;
	}


	if ($showEditTable==1){

?>

		<form name="editdepartmentform" action="departments_edit.php?id=<?php echo $_GET['id'];?>" method="POST">

			<table border="2" bgcolor="a7adc5" align="center">

				<tr>
					<td> Üzem megnevezés:</td>
					<td> <INPUT class="infield" type="text" name="Department_Name" value="<?php echo $row['Department_Name'];?>" SIZE="15" >*</td>
				</tr>
				<tr>
					<td align="right"> Módosít ></td>
					<td> <INPUT type="submit" name="kuld" value="OK" ><INPUT type="reset" name="reset" value="Alaphelyzet" >  </td>
				</tr>

			</table>

		</form>

<?php
	} 	
	//End of IF - Contitional showEditTable DISPLAY


/*********************************************
***		EVENT POST		   ***
**********************************************  */


    if($_SERVER['REQUEST_METHOD'] == "POST")
    {

	//connection to MySQL
	require_once('../db_connect.php');


	//GET FIELD VALUES INTO VARs
	//
	//UPDATE DB  ---- BASED ON NULL VALUE Handling
        
		mysqli_query($con, "UPDATE Departments SET Department_Name='{$_POST['Department_Name']}' WHERE DepartmentID='{$_GET['id']}';") ; 

        //AFTER UPDATE - PAGE Redirect to workers.php");
	echo '<script> location.replace("departments.php"); </script>';
    }


?>

</body>
</html>
