

<html>
<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	th {
	    background-color: #a5a9a4;
	}

	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}
	h1 {
			width: 50%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	#topbuttorbar, #DepWorkQuery {
		margin-bottom: 15px;
		    box-shadow: 10px 10px 20px rgba(0,0,0,.7);
	}
	.home{
		background-color: #d3d2ec;
	}

</style>
<?php

	//connection to MySQL
	require_once('../db_connect.php');

	//IF Got Delete_ID - Delete it !
	if ( isset( $_GET['delete_id'] ) )
	{
	 $sql_query="DELETE FROM Departments WHERE DepartmentID='" . $_GET['delete_id'] . "'" ;
	 mysqli_query($con, $sql_query);
	 header("Location: departments.php");
	}


echo '<head>';
echo '<title>Üzemek</title>';

echo '</head>';

echo '<body>';
        session_start();
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }


/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	//get data from table named WORKER
    	$result = $con->query("SELECT * FROM Departments ORDER BY Department_Name ASC");

	echo '<h1> Üzemek Listája';
        echo '		<button onclick="location.href= \'../logout.php\'" type="button">';
	echo ' 			 «Kijelentkezés»';
	echo '		</button>';
        echo '</h1>';

	// TOP BUTTON Blue Bar
	echo '<div id="topbuttorbar" style="background-color:#5b58e6; height:27px;">';
	echo '		<button onclick="location.href= \'departments_new.php\'    " type="button">';
	echo ' 			 + Új Üzem felvitele >>';
	echo '		</button> ║ Lekérdezés ideje: ' . date("Y-m-d") . '&nbsp &nbsp' . date("H:i:s")  ;

			//CLOCK
			//IF There is Outer Internet Connection Show TIME - else NOT
			$outerConnection = checkdnsrr('timeanddate.com') ; 
			
			if ($outerConnection == 1) {
				echo '<div style="float:right"> <iframe src="http://free.timeanddate.com/clock/i5wp1938/n3316/tlhu19/fn3/ftbi/bat2/tt0/tb2" frameborder="0" width="276" height="20"></iframe>
			</div>'; 
			} //END OF CLOCK

	echo '		<div style="float:right">';
	echo '			<button style="background-color:red" onclick="location.href= \'../WORKLIST/worklist.php\'    " type="button">';
	echo ' 				 <font size="4">«Munkák»</font>';
	echo '			</button>';
	echo '			<button style="background-color:yellow" onclick="location.href= \'../WORKDETAILS/workdetails.php\'    " type="button">';
	echo ' 				 <font size="4">«Munkák Tulajdonságai»</font>';
	echo '			</button>';
	echo '			<button style="background-color:blue" disabled onclick="location.href= \'../DEPARTMENTS/departments.php\'    " type="button">';
	echo ' 				 <font size="4">«Üzemek»</font>';
	echo '			</button>';	
	echo '			<button style="background-color:green" onclick="location.href= \'../WORKERS/workers.php\'    " type="button">';
	echo ' 				 <font size="4">«Dolgozók»</font>';
	echo '			</button>';
	echo '		</div>';


	echo '</div>';

	// TOP Worker Query BAR
	echo '<form style="background-color:#b2b1ec" name="DepWorkQuery" id="DepWorkQuery" action="dep_worklist.php">';

?>
		Üzem munkalista: <SELECT name="optionlist"> <?php 			
			while ($row = mysqli_fetch_array($result)) 
			{
				$ca = $row['DepartmentID'];

				$na = $row['Department_Name'];

				echo ' <OPTION value="' . $ca .'" > ' . $na .' &nbsp &nbsp &nbsp [' . $ca . ']  </OPTION>\n';

			} ?> </SELECT> 
		<input type="date" name="startDate">-tól 
		<input type="date" name="endDate">-ig    
		<input type="submit" value="Listázás >>" />
<?php	

	echo'</form>';

	//print Table Headers
	echo '<table border="2" bgcolor="#D4D4D4" align="center">';
	echo '    <tr>';
	echo '		<th>Üzem azonosító</th>';
	echo '		<th>Üzem megnevezés ▼ </th>';
	echo '		<th>HÉTKÖZNAPI órák</th>';
	echo '		<th>HÉTVÉGI órák</th>';
	echo '		<th>∑ Óra</th>';
	echo '		<th>∑ Díj</th>';
	echo '		<th>SZERK</th>';
	echo '		<th>TÖRÖL</th>';
	echo '   </tr>';

	//print Table Contents
    	$result = $con->query("SELECT * FROM Departments ORDER BY Department_Name ASC");
	while ($row = mysqli_fetch_array($result)) {
	
            // GET Each Department STATS - WeekDayNormalHour-WeekDayOverHour-WeekEndNormalHour-WeekEndOverHour-TOTALHOUR-TOTALCOST
            if(isset($_SESSION['startDate']) && isset($_SESSION['endDate']))
            {
                $departmentStats_result = mysqli_query($con, 
                "SELECT 
                SUM(WeekDay_WorkTime_6_18) AS WD_NH, SUM(WeekDay_OverWorkTime_18_6) AS WD_OH,
                SUM(WeekEnd_WorkTime_6_18) AS WE_NH, SUM(WeekEnd_OverWorkTime_18_6) AS WE_OH,
                SUM(WeekDay_WorkTime_6_18+WeekDay_OverWorkTime_18_6+WeekEnd_WorkTime_6_18+WeekEnd_OverWorkTime_18_6) AS SumsOf_Hour_Work,
                SUM(TotalCostOfTask) AS SumsOf_Work_TotalCOST
                FROM Worklist,Workdetails,Departments WHERE Departments.DepartmentID={$row['DepartmentID']} AND Worklist.Date BETWEEN '{$_SESSION['startDate']}' AND '{$_SESSION['endDate']}'

                AND Worklist.Work_ID=Workdetails.WorkID 
                AND Departments.DepartmentID=Worklist.Department_ID
                ");
            
            
            }else{
                $startQuery = date("Y-m-01");
                $stopQuery = date("Y-m-31");
                $departmentStats_result = mysqli_query($con, 
                "SELECT 
                SUM(WeekDay_WorkTime_6_18) AS WD_NH, SUM(WeekDay_OverWorkTime_18_6) AS WD_OH,
                SUM(WeekEnd_WorkTime_6_18) AS WE_NH, SUM(WeekEnd_OverWorkTime_18_6) AS WE_OH,
                SUM(WeekDay_WorkTime_6_18+WeekDay_OverWorkTime_18_6+WeekEnd_WorkTime_6_18+WeekEnd_OverWorkTime_18_6) AS SumsOf_Hour_Work,
                SUM(TotalCostOfTask) AS SumsOf_Work_TotalCOST
                FROM Worklist,Workdetails,Departments WHERE Departments.DepartmentID={$row['DepartmentID']} AND Worklist.Date BETWEEN '{$startQuery}' AND '{$stopQuery}'

                AND Worklist.Work_ID=Workdetails.WorkID 
                AND Departments.DepartmentID=Worklist.Department_ID
                ");
            }
            $departmentStats_row= mysqli_fetch_array($departmentStats_result);
            
	    print "<tr align='center'><td>" . $row['DepartmentID'] . "</td>";
	    print "<td>" . $row['Department_Name'] . "</td>";
	    print "<td> Normál:<u><b>".$departmentStats_row['WD_NH']."</b></u> Túlóra:<u><b>".$departmentStats_row['WD_OH']."</b></u></td>";
	    print "<td> Normál:<u><b>".$departmentStats_row['WE_NH']."</b></u> Túlóra:<u><b>".$departmentStats_row['WE_OH']."</b></u></td>";
	    print "<td><b>".$departmentStats_row['SumsOf_Hour_Work']."</b></td>";
	    print "<td><b>".$departmentStats_row['SumsOf_Work_TotalCOST']."</b></td>";
	    print '<td><a href="departments_edit.php?id=' . $row["DepartmentID"] . '"><font size="15">✎</font></a></td>';
?>
<td>  <a href="departments.php?delete_id=<?php echo $row['DepartmentID']; ?>" onclick="return confirm(' <?php echo 'Törölje \n Üzem: ' . $row['Department_Name'] . '\n Üzem azon.: [ ' . $row['DepartmentID'] . ' ]';   ?> -t ?    '); " ><font size="15">✖</font></a>  </td></tr>
<?php
	}

	echo '</table>';

	
	//close Connection
	mysqli_free_result($result);
	mysqli_close($con);



?>
</body>
</html>
