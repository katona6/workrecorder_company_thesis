<html>

<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}

	h1 {
			width: 60%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	.infield{
			
		    border-bottom-right-radius: 20% 50%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	.longinfield{
			border-top-left-radius: 10% 40%;
		    border-bottom-right-radius: 10% 40%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	.notValidNumber{
            background-color: red;
	}
	
	fieldset {
            border:2px solid #999;
            border-radius:8px;
            box-shadow:0 0 20px #999;
            background-color: #fbfbc2;
        }
        legend {
            background:#fff;
            border: grey 5px outset;
        }
	
</style>

<?php
        session_start();
	//connection to MySQL
	require_once('../db_connect.php'); 
	
        //************************************************************
        //          P H P   F U N C T I O N S
        //************************************************************
        
        function isWeekend($date) 
        {
            $weekDay = date('w', strtotime($date));
            return ($weekDay == 0 || $weekDay == 6);
        }
        

?>
<head>
<title>Dolgozó Munkához rendelése</title>


</head>
<body>
<script type="text/javascript">

    function calculate1(){
        var hour1 = parseInt(document.getElementById('hour1').value);
        if (hour1 < 0 || hour1 > 12){
            document.getElementById("hour1").classList.add('notValidNumber');
            alert('0..12 közt kell lennie!');
        }else{
            document.getElementById("hour1").classList.remove('notValidNumber');
        }
        var hourlyrate1 = parseInt(document.getElementById('hourlyrate1').value);
        var hour2 = parseInt(document.getElementById('hour2').value);
        var hourlyrate2 = parseInt(document.getElementById('hourlyrate2').value);
        var material_weekday = parseInt(document.getElementById('material_weekday').value);
        var sum=0;
        sum=hour1 * hourlyrate1 ;
        document.getElementById('calc1').value = sum;
        document.getElementById('total1').value = sum + (hour2 * hourlyrate2) + material_weekday ;
    }
    function calculate2(){
        var hour1 = parseInt(document.getElementById('hour1').value);
        var hourlyrate1 = parseInt(document.getElementById('hourlyrate1').value);
        var hour2 = parseInt(document.getElementById('hour2').value);
        if (hour2 < 0 || hour2 > 12){
            document.getElementById("hour2").classList.add('notValidNumber');
            alert('0..12 közt kell lennie!');
        }else{
            document.getElementById("hour2").classList.remove('notValidNumber');
        }
        var hourlyrate2 = parseInt(document.getElementById('hourlyrate2').value);
        var material_weekday = parseInt(document.getElementById('material_weekday').value);
        var sum=0;
        sum=hour2 * hourlyrate2 ;
        document.getElementById('calc2').value = sum;
        document.getElementById('total1').value = sum + (hour1 * hourlyrate1) + material_weekday ;
    }
    function calculate3(){
        var hour3 = parseInt(document.getElementById('hour3').value);
        if (hour3 < 0 || hour3 > 12){
            document.getElementById("hour3").classList.add('notValidNumber');
            alert('0..12 közt kell lennie!');
        }else{
            document.getElementById("hour3").classList.remove('notValidNumber');
        }
        var hourlyrate3 = parseInt(document.getElementById('hourlyrate3').value);
        var hour4 = parseInt(document.getElementById('hour4').value);
        var hourlyrate4 = parseInt(document.getElementById('hourlyrate4').value);
        var material_weekend = parseInt(document.getElementById('material_weekend').value);
        var sum=0;
        sum=hour3 * hourlyrate3 ;
        document.getElementById('calc3').value = sum;
        document.getElementById('total2').value = sum + (hour4 * hourlyrate4) + material_weekend ;
    }
    function calculate4(){
        var hour3 = parseInt(document.getElementById('hour3').value);
        var hourlyrate3 = parseInt(document.getElementById('hourlyrate3').value);
        var hour4 = parseInt(document.getElementById('hour4').value);
        if (hour4 < 0 || hour4 > 12){
            document.getElementById("hour4").classList.add('notValidNumber');
            alert('0..12 közt kell lennie!');
        }else{
            document.getElementById("hour4").classList.remove('notValidNumber');
        }
        var hourlyrate4 = parseInt(document.getElementById('hourlyrate4').value);
        var material_weekend = parseInt(document.getElementById('material_weekend').value);
        var sum=0;
        sum=hour4 * hourlyrate4 ;
        document.getElementById('calc4').value = sum;
        document.getElementById('total2').value = sum + (hour3 * hourlyrate3) + material_weekend ;
    }

</script>


<?php
        
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }

/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	echo '<h1> Dolgozó Munkájának szerk.></h1>';
	
	// BACK TO DEPARTMENTS Button - opens previous Page
	echo '<form action="workdetails.php">';
	echo '    <input type="submit" value="<< Vissza" />';
	echo '</form>';
	
	$showEditTable = 0 ;	// DISABLE Form by Default

	//GET Editable fields by ID from Database
	$id = $_GET['id'];
	$workdetail_result = mysqli_query($con,"SELECT * FROM Worklist,Workdetails,Departments WHERE Workdetails.WorkDetailID='{$id}' AND Worklist.Department_ID=Departments.DepartmentID AND Workdetails.WorkID=Worklist.Work_ID");
	$workdetail_row = mysqli_fetch_array($workdetail_result);
	
	$worker_result = mysqli_query($con,"SELECT WorkerID,Name,ID_Card_Number,HOME_City FROM Worker");
	
	$global_date = $workdetail_row['Date'];
	
	
	if ( $workdetail_row['Order_Number'] == "") 
	{

		echo 'HIBA TÖRTÉNT <br/> Nincs ilyen Munkatulajdonság-sor-azonosító, <br/> ezért nincs mit szerkeszteni...';
	}
        if ( $workdetail_row['Order_Number'] != "")
	{
		$showEditTable=1 ;
	}

        if ($showEditTable==1){
?>              <center>
		<form style="width:80%;" name="editworkeronworkform" action="workdetails_edit.php?id=<?php echo $id;?>" method="POST" align="center">
                    <fieldset>
                        <legend><?php echo $workdetail_row['Order_Number'].', '. $workdetail_row['Equipment_ID']. ', '.$workdetail_row['Work_Description'] ;?></legend>
			<table border="1" style="width:95%"  align="center">

				<tr>
					<td colspan="6" align="right"> Munkavégzés Dátuma:</td>
					<td colspan="6"> <INPUT class="infield" type="text" name="date" SIZE="15" value="<?php echo $workdetail_row['Date'];?>" disabled></td>
				</tr>
                                <tr>
					<td colspan="6" align="right"> Üzem:</td>
					<td colspan="6"> <INPUT class="infield" type="text" name="department" SIZE="15" value="<?php echo $workdetail_row['Department_Name'];?>" disabled></td>
				</tr>
				<tr>
					<td colspan="6" align="right"> Válasszon Dolgozót ></td>
					<td colspan="6"> 
                                            <select size="10" name="workerselect">
                                                <?php
                                                    while ($worker_row = mysqli_fetch_array($worker_result))
                                                    {
                                                        if($workdetail_row['WorkerID'] == $worker_row['WorkerID'])
                                                        {
                                                            echo '<option value="'.$worker_row['WorkerID'].'" selected> '.$worker_row['Name'].', ['.$worker_row['ID_Card_Number'].'] ,'.$worker_row['HOME_City'].'</option>' ;
                                                        }else{
                                                            echo '<option value="'.$worker_row['WorkerID'].'"> '.$worker_row['Name'].', ['.$worker_row['ID_Card_Number'].'] ,'.$worker_row['HOME_City'].'</option>' ;
                                                        }
                                                    
                                                    }
                                                ?>
                                            </select>
					
					</td>
				</tr>
				<tr>
					<td colspan="6" align="right"> Beosztás ></td>
					<td colspan="6"> 
                                            <select name="workingrankselect">
<?php                                           
                                                echo "<option value='{$workdetail_row['Working_Rank']}' selected>{$workdetail_row['Working_Rank']}</option>";
                                                echo '<option value="Gépszerelö">Gépszerelö</option>';
                                                echo '<option value="Ivhegesztö">Ivhegesztö</option>';
                                                echo '<option value="Hegesztö">Hegesztö</option>';
                                                echo '<option value="Esztergályos">Esztergályos</option>';
                                                echo '<option value="Targoncás">Targoncás</option>';
                                                echo '<option value="Raktáros">Raktáros</option>';
                                                echo '<option value="Könyvelö">Könyvelö</option>';
                                                echo '<option value="Munkavezetö">Munkavezetö</option>';
?>
                                            </select>
					
					</td>
				</tr>				
				<tr>
					<td colspan="6" align="center"> HÉTKÖZNAP </td>
					<td colspan="6" align="center" style="background-color:#d40000"> HÉTVÉGE </td>
				</tr>
                                <tr>
                                    
                                        
                                        
					<td colspan="3"  align="center" style="background-color:yellow"> Normál Óra <br/> Reggel 6:00-tól Este 18:00-ig </td>
					<td colspan="3"  align="center" style="background-color:#026592"> Túlóra <br/> Este 18:00-tól Reggel 6:00-ig </td>
					<td colspan="3"  align="center" style="background-color:yellow"> Normál Óra <br/> Reggel 6:00-tól Este 18:00-ig </td>
					<td colspan="3"  align="center" style="background-color:#026592"> Túlóra <br/> Este 18:00-tól Reggel 6:00-ig </td>
					
                                    
				</tr>

				<tr>
                                    <?php
                                        if(isWeekend($workdetail_row['Date']) == 1)       //IF WEEKeND THEN WeekDay DISABLED - WEEKeND ENABLED
                                        {
					echo '<td> Munkaóra <br/>szám:</td>' ;
					echo '<td> <INPUT class="infield" type="text" name="hour1" id="hour1" SIZE="5" value="0" disabled></td>' ;
					echo '<td align="right"> óra</td>' ;
					echo '<td> Munkaóra <br/>szám:</td>' ;
					echo '<td> <INPUT class="infield" type="text" name="hour2" id="hour2" SIZE="5" value="0" disabled></td>' ;
					echo '<td align="right"> óra</td>' ;
                                        echo '<td> Munkaóra <br/>szám:</td>' ;
					echo '<td> <INPUT class="infield" type="text" name="hour3" id="hour3" SIZE="5" value="'.$workdetail_row['WeekEnd_WorkTime_6_18'].'" onblur="calculate3();"></td>' ;
					echo '<td align="right"> óra</td>' ;
					echo '<td> Munkaóra <br/>szám:</td>' ;
					echo '<td> <INPUT class="infield" type="text" name="hour4" id="hour4" SIZE="5" value="'.$workdetail_row['WeekEnd_OverWorkTime_18_6'].'" onblur="calculate4();"></td>' ;
					echo '<td align="right"> óra</td>' ;
					}else
					{
					echo '<td> Munkaóra <br/>szám:</td>' ;
					echo '<td> <INPUT class="infield" type="text" name="hour1" id="hour1" SIZE="5" value="'.$workdetail_row['WeekDay_WorkTime_6_18'].'" onblur="calculate1();"></td>' ;
					echo '<td align="right"> óra</td>' ;
					echo '<td> Munkaóra <br/>szám:</td>' ;
					echo '<td> <INPUT class="infield" type="text" name="hour2" id="hour2" SIZE="5" value="'.$workdetail_row['WeekDay_OverWorkTime_18_6'].'" onblur="calculate2();"></td>' ;
					echo '<td align="right"> óra</td>' ;
                                        echo '<td> Munkaóra <br/>szám:</td>' ;
					echo '<td> <INPUT class="infield" type="text" name="hour3" id="hour3" SIZE="5" value="0" disabled></td>' ;
					echo '<td align="right"> óra</td>' ;
					echo '<td> Munkaóra <br/>szám:</td>' ;
					echo '<td> <INPUT class="infield" type="text" name="hour4" id="hour4" SIZE="5" value="0" disabled></td>' ;
					echo '<td align="right"> óra</td>' ;					
					
					}
                                    ?>
				</tr>
                                <tr>
                                    <?php
                                        if(isWeekend($workdetail_row['Date']) == 1) //IF WEEKeND THEN WeekDay DISABLED - WEEKeND ENABLED
                                        {
                                        echo '<td> Órabér:</td>' ;
                                        echo '<td> <INPUT class="infield" type="text" name="hourlyrate1" id="hourlyrate1" SIZE="10" value="0" disabled></td>' ;
                                        echo '<td align="center"> Ft/óra</td>' ;
                                        echo '<td> Órabér:</td>' ;
                                        echo '<td> <INPUT class="infield" type="text" name="hourlyrate2" id="hourlyrate2" SIZE="10" value="0" disabled></td>' ;
                                        echo '<td align="center"> Ft/óra</td>' ;
                                        echo '<td> Órabér:</td> ' ;
                                        echo '<td> <INPUT class="infield" type="text" name="hourlyrate3" id="hourlyrate3" SIZE="10" value="'.$workdetail_row['WeekEnd_HourlyRate_6_18'].'" onblur="calculate3();"></td>' ;
                                        echo '<td align="center"> Ft/óra</td>' ;
                                        echo '<td> Órabér:</td>' ;
                                        echo '<td> <INPUT class="infield" type="text" name="hourlyrate4" id="hourlyrate4" SIZE="10" value="'.$workdetail_row['WeekEnd_OverHourlyRate_18_6'].'" onblur="calculate4();"></td>' ;
                                        echo '<td align="center"> Ft/óra</td>' ;
                                        }else{
                                        echo '<td> Órabér:</td>' ;
                                        echo '<td> <INPUT class="infield" type="text" name="hourlyrate1" id="hourlyrate1" SIZE="10" value="'.$workdetail_row['WeekDay_Hourly_Rate_6_18'].'" onblur="calculate1();"></td>' ;
                                        echo '<td align="center"> Ft/óra</td>' ;
                                        echo '<td> Órabér:</td>' ;
                                        echo '<td> <INPUT class="infield" type="text" name="hourlyrate2" id="hourlyrate2" SIZE="10" value="'.$workdetail_row['WeekDay_OverHourlyRate_18_6'].'" onblur="calculate2();"></td>' ;
                                        echo '<td align="center"> Ft/óra</td>' ;
                                        echo '<td> Órabér:</td> ' ;
                                        echo '<td> <INPUT class="infield" type="text" name="hourlyrate3" id="hourlyrate3" SIZE="10" value="0" disabled></td>' ;
                                        echo '<td align="center"> Ft/óra</td>' ;
                                        echo '<td> Órabér:</td>' ;
                                        echo '<td> <INPUT class="infield" type="text" name="hourlyrate4" id="hourlyrate4" SIZE="10" value="0" disabled></td>' ;
                                        echo '<td align="center"> Ft/óra</td>' ;                                        
                                        
                                        }
                                    ?>
				</tr>
                                <tr>
                                        <td style="background-color:orange"> Díj:</td>
                                        <td style="background-color:orange"> <INPUT class="infield" type="text" id="calc1" SIZE="10" value="<?php echo $workdetail_row['WeekDay_WorkTime_6_18']*$workdetail_row['WeekDay_Hourly_Rate_6_18'];?>" disabled ></td>
                                        <td style="background-color:orange"> &nbsp Ft</td>
                                        <td style="background-color:#024563"> <font color="white">Díj:</font></td>
                                        <td style="background-color:#024563"> <INPUT class="infield" type="text" id="calc2" SIZE="10" value="<?php echo $workdetail_row['WeekDay_OverWorkTime_18_6']*$workdetail_row['WeekDay_OverHourlyRate_18_6'];?>" disabled ></td>
                                        <td style="background-color:#024563"> &nbsp <font color="white">Ft</font></td>
                                        <td style="background-color:orange"> Díj:</td>
                                        <td style="background-color:orange"> <INPUT class="infield" type="text" id="calc3" SIZE="10" value="<?php echo $workdetail_row['WeekEnd_WorkTime_6_18']*$workdetail_row['WeekEnd_HourlyRate_6_18'];?>" disabled ></td>
                                        <td style="background-color:orange"> &nbsp Ft</td>
                                        <td style="background-color:#024563"> <font color="white">Díj:</font></td>
                                        <td style="background-color:#024563"> <INPUT class="infield" type="text" id="calc4" SIZE="10" value="<?php echo $workdetail_row['WeekEnd_OverWorkTime_18_6']*$workdetail_row['WeekEnd_OverHourlyRate_18_6'];?>" disabled ></td>
                                        <td style="background-color:#024563"> &nbsp<font color="white"> Ft</font></td>
				</tr>
                                <tr>
                                    <?php
                                        if(isWeekend($workdetail_row['Date']) == 1) //IF WEEKeND THEN WeekDay DISABLED - WEEKeND ENABLED
                                        {
					echo '<td colspan="6" align="center"> Anyagköltség: ' ;
                                        echo '    <INPUT class="infield" type="text" name="material_weekday" id="material_weekday" SIZE="10" value="0" disabled> Ft' ;
					echo '</td>' ;
					echo '<td colspan="6" align="center"> Anyagköltség: ' ;
                                        echo '    <INPUT class="infield" type="text" name="material_weekend" id="material_weekend" SIZE="10" value="'.$workdetail_row['MaterialCostOfTask'].'" onblur="calculate3();"> Ft' ;
					echo '</td>' ;
					}else{
					echo '<td colspan="6" align="center"> Anyagköltség: ' ;
                                        echo '    <INPUT class="infield" type="text" name="material_weekday" id="material_weekday" SIZE="10" value="'.$workdetail_row['MaterialCostOfTask'].'" onblur="calculate1();"> Ft' ;
					echo '</td>' ;
					echo '<td colspan="6" align="center"> Anyagköltség: ' ;
                                        echo '    <INPUT class="infield" type="text" name="material_weekend" id="material_weekend" SIZE="10" value="0" disabled> Ft' ;
					echo '</td>' ;					
					
					}
                                    ?>
				</tr>
                                <tr>
					<td colspan="6" align="center"> ÖSSZESEN: 
                                            <INPUT class="infield" type="text" name="total1" id="total1" SIZE="10" value="<?php echo $workdetail_row['TotalCostOfTask'];?>" disabled> Ft
					</td>
					<td colspan="6" align="center"> ÖSSZESEN: 
                                            <INPUT class="infield" type="text" name="total2" id="total2" SIZE="10" value="<?php echo $workdetail_row['TotalCostOfTask'];?>" disabled> Ft
					</td>
				</tr>

				<tr>
					<td colspan="6" align="right"> Dolgozó Munka-módosítása ></td>
					<td colspan="6"> <INPUT type="submit" name="kuld" value="OK" >&nbsp &nbsp &nbsp &nbsp &nbsp<INPUT type="reset" name="reset" value="Alaphelyzet" >  </td>
				</tr>

			</table> &nbsp
                    </fieldset>
		</form>
		</center>

<?php
        }//END OF Conditional FORM Print
        
        
        
        
        

    if($_SERVER['REQUEST_METHOD'] == "POST")
    {

	//connection to MySQL
	require_once('../db_connect.php'); 

        
	//GET FIELD VALUES INTO VARs
        $workerid = $_POST['workerselect'];
        //workid from URL GET
        $working_rank = $_POST['workingrankselect'];
        
        if(isWeekend($global_date) == 1){           //IF WEEKend
            //$material_wd = $_POST['material_weekday'];
            $material_we = $_POST['material_weekend'];
            //$hour1 = $_POST['hour1'];
            //$hour2 = $_POST['hour2'];
            $hour3 = $_POST['hour3'];
            $hour4 = $_POST['hour4'];
            //$hourlyrate1 = $_POST['hourlyrate1'];
            //$hourlyrate2 = $_POST['hourlyrate2'];
            $hourlyrate3 = $_POST['hourlyrate3'];
            $hourlyrate4 = $_POST['hourlyrate4'];
            
            //INSERT INTO DB
            mysqli_query($con, "UPDATE Workdetails SET WorkerID='$workerid',Working_Rank='$working_rank',MaterialCostOfTask=$material_we,WeekDay_WorkTime_6_18=0,WeekDay_Hourly_Rate_6_18=0,WeekDay_OverWorkTime_18_6=0,WeekDay_OverHourlyRate_18_6=0,WeekEnd_WorkTime_6_18=$hour3,WeekEnd_HourlyRate_6_18=$hourlyrate3,WeekEnd_OverWorkTime_18_6=$hour4,WeekEnd_OverHourlyRate_18_6=$hourlyrate4 WHERE WorkDetailID='$id'") ;
            
        }else{
            $material_wd = $_POST['material_weekday'];
            //$material_we = $_POST['material_weekend'];
            $hour1 = $_POST['hour1'];
            $hour2 = $_POST['hour2'];
            //$hour3 = $_POST['hour3'];
            //$hour4 = $_POST['hour4'];
            $hourlyrate1 = $_POST['hourlyrate1'];
            $hourlyrate2 = $_POST['hourlyrate2'];
            //$hourlyrate3 = $_POST['hourlyrate3'];
            //$hourlyrate4 = $_POST['hourlyrate4'];
            
            //INSERT INTO DB
            mysqli_query($con, "UPDATE Workdetails SET WorkerID='$workerid',Working_Rank='$working_rank',MaterialCostOfTask=$material_wd,WeekDay_WorkTime_6_18=$hour1,WeekDay_Hourly_Rate_6_18=$hourlyrate1,WeekDay_OverWorkTime_18_6=$hour2,WeekDay_OverHourlyRate_18_6=$hourlyrate2,WeekEnd_WorkTime_6_18=0,WeekEnd_HourlyRate_6_18=0,WeekEnd_OverWorkTime_18_6=0,WeekEnd_OverHourlyRate_18_6=0 WHERE WorkDetailID='$id'") ;
        
        
        }

	echo '<script> location.replace("workdetails.php"); </script>';
	
    }       //end of POST TO DB


?>

</body>
</html>
