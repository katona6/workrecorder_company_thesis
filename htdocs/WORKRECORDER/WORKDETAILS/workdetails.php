
<?php
        session_start();
	//connection to MySQL
	require_once('../db_connect.php');

	//IF Got Delete_ID - Delete it !
	if ( isset( $_GET['delete_id'] ) )
	{
	 $sql_query="DELETE FROM Workdetails WHERE WorkDetailID='" . $_GET['delete_id'] . "'" ;
	 mysqli_query($con, $sql_query);
	 header("Location: workdetails.php");
	}

	
	// IF Got DATE FILTER - Filter it !
	$dateFilter = 0;
	if ( isset($_SESSION['startDate']) && isset($_SESSION['endDate']) )
	{
            $dateFilter = 1;
	}
	
	//************************************************************
        //          P H P   F U N C T I O N S
        //************************************************************
        
        function isWeekend($date) 
        {
            $weekDay = date('w', strtotime($date));
            return ($weekDay == 0 || $weekDay == 6);
        }

        
        
?>        
        
        
        

<html>
<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	th {
	    background-color: #a5a9a4;
	}

	table {
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}
	h1 {
			width: 80%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	#topbuttorbar, #topworkerqueryBAR {
		margin-bottom: 15px;
		    box-shadow: 10px 10px 20px rgba(0,0,0,.7);
	}
	.home{
		background-color: #d3d2ec;
	}
	.hidden{
            display: none;
	}
	

</style>

<?php
echo '<head>';
echo '  <title>Munkatulajdonságok</title>';
echo '  <script src="../jquery_321.min.js"></script>';
echo '</head>';
?>

<script language="JavaScript" type="text/javascript" >


            function showHide(ele){
            
                $("#"+ele.id+"_ROW").toggle(500);
            };

</script>

<?php
echo '<body>';
        
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }


/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	//get data from table named WORKLIST - Conditional - if filtered
	
	if ($dateFilter == 1)
	{
            $result = $con->query("SELECT Work_ID,Date,Order_Number,Equipment_ID,Work_Description,Department_Name FROM Worklist,Departments WHERE Department_ID=DepartmentID AND Date BETWEEN '{$_SESSION['startDate']}' AND '{$_SESSION['endDate']}' ORDER BY Date, Department_Name");
            
            $normal_WorkTime = $con->query("SELECT SUM(WeekDay_WorkTime_6_18) + SUM(WeekEnd_WorkTime_6_18) AS Normal_WorkTime FROM Workdetails,Worklist WHERE Date BETWEEN '{$_SESSION['startDate']}' AND '{$_SESSION['endDate']}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_normal_WorkTime = mysqli_fetch_array($normal_WorkTime);
            
            $over_WorkTime = $con->query("SELECT SUM(WeekDay_OverWorkTime_18_6) + SUM(WeekEnd_OverWorkTime_18_6) AS Over_WorkTime FROM Workdetails,Worklist WHERE Date BETWEEN '{$_SESSION['startDate']}' AND '{$_SESSION['endDate']}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_over_WorkTime = mysqli_fetch_array($over_WorkTime);
            
            $allWorkingFee = $con->query("SELECT SUM(TotalCostOfTask) - SUM(MaterialCostOfTask) AS All_Working_Fee FROM Workdetails,Worklist WHERE Date BETWEEN '{$_SESSION['startDate']}' AND '{$_SESSION['endDate']}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_allWorkingFee = mysqli_fetch_array($allWorkingFee);
            
            $allMaterialCost = $con->query("SELECT SUM(MaterialCostOfTask) AS All_Material_Cost FROM Workdetails,Worklist WHERE Date BETWEEN '{$_SESSION['startDate']}' AND '{$_SESSION['endDate']}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_allMaterialCost = mysqli_fetch_array($allMaterialCost);
            
            $allTaskCost = $con->query("SELECT SUM(TotalCostOfTask) AS All_Task_Cost FROM Workdetails,Worklist WHERE Date BETWEEN '{$_SESSION['startDate']}' AND '{$_SESSION['endDate']}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_allTaskCost = mysqli_fetch_array($allTaskCost);
	}
	else
	{
            //IF FILTER is NOT activated - Query the ACTUAL Month Works !!
            $startQuery = date("Y-m-01");
            $stopQuery = date("Y-m-31");
            
            $result = $con->query("SELECT Work_ID,Date,Order_Number,Equipment_ID,Work_Description,Department_Name FROM Worklist,Departments WHERE Department_ID=DepartmentID AND Date BETWEEN '{$startQuery}' AND '{$stopQuery}' ORDER BY Date, Department_Name");
            
            $normal_WorkTime = $con->query("SELECT SUM(WeekDay_WorkTime_6_18) + SUM(WeekEnd_WorkTime_6_18) AS Normal_WorkTime FROM Workdetails,Worklist WHERE Date BETWEEN '{$startQuery}' AND '{$stopQuery}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_normal_WorkTime = mysqli_fetch_array($normal_WorkTime);
            
            $over_WorkTime = $con->query("SELECT SUM(WeekDay_OverWorkTime_18_6) + SUM(WeekEnd_OverWorkTime_18_6) AS Over_WorkTime FROM Workdetails,Worklist WHERE Date BETWEEN '{$startQuery}' AND '{$stopQuery}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_over_WorkTime = mysqli_fetch_array($over_WorkTime);
            
            $allWorkingFee = $con->query("SELECT SUM(TotalCostOfTask) - SUM(MaterialCostOfTask) AS All_Working_Fee FROM Workdetails,Worklist WHERE Date BETWEEN '{$startQuery}' AND '{$stopQuery}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_allWorkingFee = mysqli_fetch_array($allWorkingFee);
            
            $allMaterialCost = $con->query("SELECT SUM(MaterialCostOfTask) AS All_Material_Cost FROM Workdetails,Worklist WHERE Date BETWEEN '{$startQuery}' AND '{$stopQuery}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_allMaterialCost = mysqli_fetch_array($allMaterialCost);
            
            $allTaskCost = $con->query("SELECT SUM(TotalCostOfTask) AS All_Task_Cost FROM Workdetails,Worklist WHERE Date BETWEEN '{$startQuery}' AND '{$stopQuery}' AND Workdetails.WorkID = Worklist.Work_ID");
            $row_allTaskCost = mysqli_fetch_array($allTaskCost);            
            
    	}

	echo '<h1>Munkatulajdonságok';
        echo '		<button onclick="location.href= \'../logout.php\'" type="button">';
	echo ' 			 «Kijelentkezés»';
	echo '		</button>';
        echo '</h1>';

	// TOP BUTTON Yellow Bar
	echo '<div id="topbuttorbar" style="background-color:yellow; height:27px;">';
	echo ' ║ Lekérdezés ideje: ' . date("Y-m-d") . '&nbsp &nbsp' . date("H:i:s")  ;

			//CLOCK
			//IF There is Outer Internet Connection Show TIME - else NOT
			$outerConnection = checkdnsrr('timeanddate.com') ; 
			
			if ($outerConnection == 1) {
				echo '<div style="float:right"> <iframe src="http://free.timeanddate.com/clock/i5wp1938/n3316/tlhu19/fn3/ftbi/bat2/tt0/tb2" frameborder="0" width="276" height="20"></iframe>
			</div>'; 
			} //END OF CLOCK

	echo '		<div style="float:right">';
	echo '			<button style="background-color:red" onclick="location.href= \'../WORKLIST/worklist.php\'    " type="button">';
	echo ' 				 <font size="4">«Munkák»</font>';
	echo '			</button>';
	echo '			<button style="background-color:yellow" disabled onclick="location.href= \'../WORKDETAILS/workdetails.php\'    " type="button">';
	echo ' 				 <font size="4">«Munkák Tulajdonságai»</font>';
	echo '			</button>';
	echo '			<button style="background-color:blue" onclick="location.href= \'../DEPARTMENTS/departments.php\'    " type="button">';
	echo ' 				 <font size="4">«Üzemek»</font>';
	echo '			</button>';	
	echo '			<button style="background-color:green" onclick="location.href= \'../WORKERS/workers.php\'    " type="button">';
	echo ' 				 <font size="4">«Dolgozók»</font>';
	echo '			</button>';
	echo '		</div>';


	echo '</div>';

	// TOP Worker Query BAR
	echo '<form style="background-color:orange " name="personWorkQuery" id="topworkerqueryBAR" action="workdetails.php" method="POST" >';
	echo '	Szűrés dátumra:  ';
	
                if ( isset($_SESSION['startDate'])){
                            echo '<input type="date" name="startDate" value='.$_SESSION['startDate'].'>-tól ';
                }else{
                            echo '<input type="date" name="startDate">-tól ';
                }

                if ( isset($_SESSION['endDate'])){
                            echo '<input type="date" name="endDate" value='.$_SESSION['endDate'].'>-ig ';
                }else{
                            echo '<input type="date" name="endDate">-ig    ';
                }
	
	
	echo '	<input type="submit" name="filterPressed" value="Szűrés >>" />';
	echo '</form>';
        if (isset($_POST['startDate']) && isset($_POST['endDate'])) 
	{ 
            // PUT Filter data into Session Variables
            $_SESSION['startDate'] = $_POST['startDate'];
            $_SESSION['endDate'] = $_POST['endDate'];
            //AFTER Session FilterSET -ACTIVATE FILTER -RE-Query Data - PAGE Redirect to SELF
            echo '<script> location.replace("workdetails.php"); </script>';
        } 
        
        //Print Work Stats
        echo 'Normál Munkaórák: <b>'.$row_normal_WorkTime['Normal_WorkTime'].'</b> Túlórák:<b> '.$row_over_WorkTime['Over_WorkTime'].'</b> Összes Munkadíj:<b> '.$row_allWorkingFee['All_Working_Fee'].' Ft</b> Összes Anyagköltség: <b>'.$row_allMaterialCost['All_Material_Cost'].' Ft</b> Összes Díj: <b><u><font style="border-style: outset; border-color:grey;">'.$row_allTaskCost['All_Task_Cost'].' Ft</font></u></b>';
	//print Table Headers
	
	if ($dateFilter != 1){
            echo '<br/>Az aktuális hónap Összes Munkája:<br/>';
	}
	
	echo '<table id="MainTable" border="2" bgcolor="#D4D4D4" align="center" style="width: 100%;">';
	echo '    <tr>';
	echo '		<th>Dátum ▼ </th>';
        echo '		<th>Üzem </th>';
	echo '		<th>Megrendelés <br/> azonosító </th>';
	echo '		<th>Berendezés <br/> azonosító </th>';
	echo '		<th>Munka leírása / megnevezése </th>';
	echo '		<th>∑ Óra</th>';
	echo '		<th>∑ Díj</th>';
	echo '		<th>+</th>';
	echo '   </tr>';

	//print MAIN Table Contents
	while ($row = mysqli_fetch_array($result)) {
	
            //GET ROW STATS (Sum of Hours - Sum Of TotalCosts)
            $rowstats_result = mysqli_query($con, "SELECT SUM(WeekDay_WorkTime_6_18+WeekDay_OverWorkTime_18_6+WeekEnd_WorkTime_6_18+WeekEnd_OverWorkTime_18_6) AS SumsOf_Hour_Work, SUM(TotalCostOfTask) AS SumsOf_Work_TotalCOST FROM Worklist,Workdetails WHERE Worklist.Work_ID='{$row['Work_ID']}' AND Worklist.Work_ID=Workdetails.WorkID");
            $rowstats_result_row = mysqli_fetch_array($rowstats_result);
            
            //IF WEEKend color that row with RED
            if(isWeekend($row['Date']) == 1)
            {
                print "<tr style='cursor:pointer' align='center' bgcolor='#D40000' id='{$row["Work_ID"]}' onclick='showHide(this);'>    ";
            }else{
                print "<tr style='cursor:pointer' align='center' id='{$row["Work_ID"]}' onclick='showHide(this);'>    ";
            }
            

	    print "        <td>" . $row['Date'] . "</td>";
	    print "        <td>" . $row['Department_Name'] . "</td>";
	    print "        <td>" . $row['Order_Number'] . "</td>";
	    print "        <td>" . $row['Equipment_ID'] . "</td>";
	    print "        <td>" . $row['Work_Description'] . "</td>";
            print "    <td>".$rowstats_result_row['SumsOf_Hour_Work']."</td>";
	    print "    <td>".$rowstats_result_row['SumsOf_Work_TotalCOST']."</td>";
	    //print '<td><a href="worklist_edit.php?id=' . $row["Work_ID"] . '"><font size="15">✎</font></a></td>';
            ?>             <td>	<button onclick="location.href='workdetails_new.php?work_id=<?php echo $row["Work_ID"];?>'" type="button">+ Dolgozó Hozzáadása ></button><?php
            print "        </td>";
	    print "</tr>";
            
            
            
            //GET INSIDER TABLE DATA FROM DB
            $inresult = $con->query("SELECT WorkDetailID, Name, ID_Card_Number, HOME_City, Working_Rank, WeekDay_WorkTime_6_18,WeekDay_Hourly_Rate_6_18,WeekDay_OverWorkTime_18_6,WeekDay_OverHourlyRate_18_6,WeekEnd_WorkTime_6_18,WeekEnd_HourlyRate_6_18,WeekEnd_OverWorkTime_18_6,WeekEnd_OverHourlyRate_18_6,MaterialCostOfTask,TotalCostOfTask,WeekDay_WorkTime_6_18*WeekDay_Hourly_Rate_6_18 AS Sum1,WeekDay_OverWorkTime_18_6*WeekDay_OverHourlyRate_18_6 AS Sum2,WeekEnd_WorkTime_6_18*WeekEnd_HourlyRate_6_18 AS Sum3, WeekEnd_OverWorkTime_18_6*WeekEnd_OverHourlyRate_18_6 AS Sum4            FROM Workdetails,Worker WHERE WorkID='{$row["Work_ID"]}' AND Workdetails.WorkerID=Worker.WorkerID;");
            
            print "<tr id='{$row["Work_ID"]}_ROW' class='hidden'>";
            print "     <td colspan='8' bgcolor='darkgrey'>";
            //******************************************************************************************* INSIDE TABLE
            print "         <table   align='center' bgcolor='grey'>";
            echo '		<th style="background-color:#66bf66">Dolgozó</th>';
            echo '		<th>Beosztás </th>';
            echo '		<th style="background-color:yellow">Munkaóra <br/>(6-18) </th>';
            echo '		<th style="background-color:yellow">Órabér <br/>(6-18) </th>';
            echo '		<th style="background-color:orange">Díj <br/>(6-18) </th>';
            echo '<th>&nbsp</th>';
            echo '		<th style="background-color:#026592">Munkaóra <br/>(18-6) </th>';
            echo '		<th style="background-color:#026592">Órabér <br/>(18-6) </th>';
            echo '		<th style="background-color:#024563">Díj <br/>(18-6) </th>';
            echo '		<th style="background-color:white">Anyagktg. </th>';
            echo '<th>&nbsp</th>';
            echo '		<th>Összesen</th>';
            echo '		<th>SZERK</th>';
            echo '		<th>TÖRÖL</th>';

            
            while ($row2 = mysqli_fetch_array($inresult))
            {
                echo '		<tr>';
                echo '		    <td>   '.$row2['Name'].' ['.$row2['ID_Card_Number'].'], '.$row2['HOME_City'].' </td>';
                echo '		    <td>   '.$row2['Working_Rank'].'  </td>';
                
                if(isWeekend($row['Date']) == 1)
                { //IF WEEKend --> show the Weekend columns FROM DB
                echo '		    <td align="right">   '.$row2['WeekEnd_WorkTime_6_18'].' óra </td>';
                echo '		    <td align="center">   '.$row2['WeekEnd_HourlyRate_6_18'].' Ft/óra  </td>';
                echo '		    <td align="right"><b> '. $row2['Sum3'] .' Ft  </b></td>';
                echo '		    <td>   </td>';
                echo '		    <td align="right">   '.$row2['WeekEnd_OverWorkTime_18_6'].' óra </td>';
                echo '		    <td align="center">   '.$row2['WeekEnd_OverHourlyRate_18_6'].' Ft/óra  </td>';
                echo '		    <td align="right"><b>  '.$row2['Sum4'].' Ft  </b></td>';
                }else{//IF NOT WEEKend --> show the Weekday columns FROM DB
                echo '		    <td align="right">   '.$row2['WeekDay_WorkTime_6_18'].' óra </td>';
                echo '		    <td align="center">   '.$row2['WeekDay_Hourly_Rate_6_18'].' Ft/óra  </td>';
                echo '		    <td align="right"><b> '. $row2['Sum1'] .' Ft  </b></td>';
                echo '		    <td>   </td>';
                echo '		    <td align="right">   '.$row2['WeekDay_OverWorkTime_18_6'].' óra </td>';
                echo '		    <td align="center">   '.$row2['WeekDay_OverHourlyRate_18_6'].' Ft/óra  </td>';
                echo '		    <td align="right"><b>  '.$row2['Sum2'].' Ft  </b></td>';
                }
                
                echo '		    <td align="right"><i>   '.$row2['MaterialCostOfTask'].' Ft </i></td>';
                echo '		    <td>  </td>';
                echo '		    <td>  <font size=4><u><b> '.$row2['TotalCostOfTask'].'</b></u></font> Ft</td>';
                echo '		    <td align="center">  <a href="workdetails_edit.php?id=' . $row2["WorkDetailID"] . '"><font size="5">✎</font></a> </td>';
?>
                                    <td align="center">  <a href="workdetails.php?delete_id=<?php echo $row2['WorkDetailID']; ?>" onclick="return confirm(' <?php echo 'Törölje \n' . $row2['Name'] . ' [ ' . $row2['ID_Card_Number'] . ' ]';   ?> \nmunkáját ?    '); " ><font size="5">✖</font></a>  </td>
<?php
                echo '		</tr>';
            }



            print "         </table> &nbsp <br/> &nbsp";
            //print isWeekend($row['Date']);
            //******************************************************************************************* END INSIDE TABLE
            print "     </td>";
            print "</tr>";
	}

	echo '</table>';

	
	//close Connection
	mysqli_free_result($result);
	mysqli_close($con);



?>
</body>
</html>
