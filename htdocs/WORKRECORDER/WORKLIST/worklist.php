
<?php
        session_start();
	//connection to MySQL
	require_once('../db_connect.php');

	//IF Got Delete_ID - Delete it !
	if ( isset( $_GET['delete_id'] ) )
	{
	 $sql_query="DELETE FROM Worklist WHERE Work_ID='" . $_GET['delete_id'] . "'" ;
	 mysqli_query($con, $sql_query);
	 header("Location: worklist.php");
	}
	
	// IF Got DATE FILTER - Filter it !
	$dateFilter = 0;
	if ( isset($_SESSION['startDate']) && isset($_SESSION['endDate']) )
	{
            $dateFilter = 1;
	}
        
        //************************************************************
        //          P H P   F U N C T I O N S
        //************************************************************
        
        function isWeekend($date) 
        {
            $weekDay = date('w', strtotime($date));
            return ($weekDay == 0 || $weekDay == 6);
        }
?>
<html>
<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	th {
	    background-color: #a5a9a4;
	}

	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
                    width: 100%;
	}
	h1 {
			width: 80%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	#topbuttorbar, #WorklistFilter {
		margin-bottom: 15px;
		    box-shadow: 10px 10px 20px rgba(0,0,0,.7);
	}
	.home{
		background-color: #d3d2ec;
	}

</style>
        
        
        
        
        

<?php

echo '<head>';
echo '<title>Munkák listája</title>';

echo '</head>';

echo '<body>';
        
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }


/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	//get data from table named WORKER
	
	if ($dateFilter == 1)
	{
            $result = $con->query("SELECT Work_ID,Date,Order_Number,Equipment_ID,Work_Description,Department_Name FROM Worklist,Departments WHERE Department_ID=DepartmentID AND Date BETWEEN '{$_SESSION['startDate']}' AND '{$_SESSION['endDate']}' ORDER BY Date, Department_Name");
	}
	else
	{
            //IF FILTER is NOT activated - Query the ACTUAL Month Works !!
            $startQuery = date("Y-m-01");
            $stopQuery = date("Y-m-31");
            $result = $con->query("SELECT Work_ID,Date,Order_Number,Equipment_ID,Work_Description,Department_Name FROM Worklist,Departments WHERE Department_ID=DepartmentID AND Date BETWEEN '{$startQuery}' AND '{$stopQuery}' ORDER BY Date, Department_Name");
    	}

	echo '<h1>Munkák Listája';
        echo '		<button onclick="location.href= \'../logout.php\'" type="button">';
	echo ' 			 «Kijelentkezés»';
	echo '		</button>';
        echo '</h1>';

	// TOP BUTTON Red Bar
	echo '<div id="topbuttorbar" style="background-color:#de2626; height:27px;">';
	echo '		<button onclick="location.href= \'worklist_new.php\'    " type="button">';
	echo ' 			 + Új Munka felvitele >>';
	echo '		</button> ║ Lekérdezés ideje: ' . date("Y-m-d") . '&nbsp &nbsp' . date("H:i:s")  ;

			//CLOCK
			//IF There is Outer Internet Connection Show TIME - else NOT
			$outerConnection = checkdnsrr('timeanddate.com') ; 
			
			if ($outerConnection == 1) {
				echo '<div style="float:right"> <iframe src="http://free.timeanddate.com/clock/i5wp1938/n3316/tlhu19/fn3/ftbi/bat2/tt0/tb2" frameborder="0" width="276" height="20"></iframe>
			</div>'; 
			} //END OF CLOCK

	echo '		<div style="float:right">';
	echo '			<button style="background-color:red" disabled onclick="location.href= \'../WORKLIST/worklist.php\'    " type="button">';
	echo ' 				 <font size="4">«Munkák»</font>';
	echo '			</button>';
	echo '			<button style="background-color:yellow" onclick="location.href= \'../WORKDETAILS/workdetails.php\'    " type="button">';
	echo ' 				 <font size="4">«Munkák Tulajdonságai»</font>';
	echo '			</button>';
	echo '			<button style="background-color:blue" onclick="location.href= \'../DEPARTMENTS/departments.php\'    " type="button">';
	echo ' 				 <font size="4">«Üzemek»</font>';
	echo '			</button>';	
	echo '			<button style="background-color:green" onclick="location.href= \'../WORKERS/workers.php\'    " type="button">';
	echo ' 				 <font size="4">«Dolgozók»</font>';
	echo '			</button>';
	echo '		</div>';


	echo '</div>';

	// TOP Worklist Query BAR
	echo '<form style="background-color:#e06b6b" name="WorklistFilter" id="WorklistFilter" action="worklist.php" method="POST" >';
	echo '	Szűrés dátumra:  ';
	
                if ( isset($_SESSION['startDate'])){
                            echo '<input type="date" name="startDate" value='.$_SESSION['startDate'].'>-tól ';
                }else{
                            echo '<input type="date" name="startDate">-tól ';
                }

                if ( isset($_SESSION['endDate'])){
                            echo '<input type="date" name="endDate" value='.$_SESSION['endDate'].'>-ig ';
                }else{
                            echo '<input type="date" name="endDate">-ig    ';
                }
	
	echo '	<input type="submit" name="filterPressed" value="Szűrés >>" />';
	echo '</form>';
	
	if (isset($_POST['startDate']) && isset($_POST['endDate'])) 
	{ 
            // PUT Filter data into Session Variables
            $_SESSION['startDate'] = $_POST['startDate'];
            $_SESSION['endDate'] = $_POST['endDate'];
            //AFTER Session FilterSET -ACTIVATE FILTER -RE-Query Data - PAGE Redirect to SELF
            echo '<script> location.replace("worklist.php"); </script>';
        } 
        
        
        if ($dateFilter != 1){
            echo 'Az aktuális hónap Összes Munkája:<br/>';
	}
	//print Table Headers
	echo '<table border="2" bgcolor="#D4D4D4" align="center">';
	echo '    <tr>';
	echo '		<th>Dátum ▼ </th>';
        echo '		<th>Üzem </th>';
	echo '		<th>Megrendelés <br/> azonosító </th>';
	echo '		<th>Berendezés <br/> azonosító </th>';
	echo '		<th>Munka leírása / megnevezése </th>';
	echo '		<th>∑ Óra</th>';
	echo '		<th>∑ Díj</th>';
	echo '		<th>SZERK</th>';
	echo '		<th>TÖRÖL</th>';
	echo '   </tr>';

	//print Table Contents
	while ($row = mysqli_fetch_array($result)) {
	
            //GET ROW STATS (Sum of Hours - Sum Of TotalCosts)
            $rowstats_result = mysqli_query($con, "SELECT SUM(WeekDay_WorkTime_6_18+WeekDay_OverWorkTime_18_6+WeekEnd_WorkTime_6_18+WeekEnd_OverWorkTime_18_6) AS SumsOf_Hour_Work, SUM(TotalCostOfTask) AS SumsOf_Work_TotalCOST FROM Worklist,Workdetails WHERE Worklist.Work_ID='{$row['Work_ID']}' AND Worklist.Work_ID=Workdetails.WorkID");
            $rowstats_result_row = mysqli_fetch_array($rowstats_result);
            
            //IF WEEKend color that row with RED
            if(isWeekend($row['Date']) == 1)
            {
                print "<tr align='center' bgcolor='#D40000'>    ";
            }else{
                print "<tr align='center'>    ";
            }
	    print "    <td>" . $row['Date'] . "</td>";
	    print "    <td>" . $row['Department_Name'] . "</td>";
	    print "    <td>" . $row['Order_Number'] . "</td>";
	    print "    <td>" . $row['Equipment_ID'] . "</td>";
	    print "    <td>" . $row['Work_Description'] . "</td>";
            print "    <td>".$rowstats_result_row['SumsOf_Hour_Work']."</td>";
	    print "    <td>".$rowstats_result_row['SumsOf_Work_TotalCOST']."</td>";
	    print '    <td><a href="worklist_edit.php?id=' . $row["Work_ID"] . '"><font size="5">✎</font></a></td>';
	    
	    if(($rowstats_result_row['SumsOf_Hour_Work']) != '')
	    {
                print "<td> <a href='#' title='** NEM-ÜRES MUNKA! MÉG NEM TÖRÖLHETI! **\nEgy vagy több Dolgozó hozzá van rendelve ehhez...\nElőbb a Dolgozókat törölje ezen munka alól a\nMUNKÁK TULAJDONSÁGAI menüben!'><font size='5'>✖</font></a> </td>  </tr>";
	    }else{
	    
	    
	    
?>
<td>  <a href="worklist.php?delete_id=<?php echo $row['Work_ID']; ?>" onclick="return confirm(' <?php echo 'Törölje \n Munka: ' . $row['Work_Description'] . '\n Üzem : [ ' . $row['Department_Name'] . ' ]';   ?> -t ?    '); " ><font size="5">✖</font></a>  </td>
                    </tr>
<?php
        }
	}

	echo '</table>';

	
	//close Connection
	mysqli_free_result($result);
	mysqli_close($con);



?>
</body>
</html>
