<html>

<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}

	h1 {
			width: 50%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	.infield{
			border-top-left-radius: 30% 30%;
		    border-bottom-right-radius: 20% 50%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	.longinfield{
			border-top-left-radius: 10% 40%;
		    border-bottom-right-radius: 10% 40%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	
</style>

<?php

	//connection to MySQL
	require_once('../db_connect.php');
	$result = mysqli_query($con,"SELECT * FROM Departments");

?>
<head>
<title>Munka-adat módosítás</title>


</head>
<body>



<?php
        session_start();
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }

/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	echo '<h1> Munka adatainak módosítása </h1>';
	
	// BACK TO WORKERS Button - opens previous Page
	echo '<form action="worklist.php">';
	echo '    <input type="submit" value="<< Vissza" />';
	echo '</form>';
	
	$showEditTable = 0 ;	// DISABLE Editor by Default

	//GET Editable fields by ID from Database
	$result = mysqli_query($con,"SELECT * FROM Worklist WHERE Work_ID='{$_GET['id']}'");
	$row = mysqli_fetch_array($result);
	if ( $row['Order_Number'] == "") 
	{

		echo 'HIBA TÖRTÉNT <br/> Nincs ilyen Munka-azonosító, <br/> ezért nincs mit szerkeszteni...';
	}
        if ( $row['Order_Number'] != "")
	{
		$showEditTable=1 ;
	}
	
	
	if ($showEditTable==1){

?>
		<form name="worklist_edit_form" action="worklist_edit.php?id=<?php echo $_GET['id'];?>" method="POST">

			<table border="2" bgcolor="a7adc5" align="center">

				<tr>
					<td> Munkavégzés Dátuma:</td>
					<td> <INPUT class="infield" type="date" name="Date" value="<?php echo $row['Date'];?>" SIZE="15" ></td>
				</tr>

				<tr>
					<td> Megrendelés azonosító:</td>
					<td> <INPUT class="infield" type="text" name="Order_Number" value="<?php echo $row['Order_Number'];?>" SIZE="15" ></td>
				</tr>

				<tr>
					<td> Berendezés azonosító:</td>
					<td> <INPUT class="infield" type="text" name="Equipment_ID" value="<?php echo $row['Equipment_ID'];?>" SIZE="15" ></td>
				</tr>



				<tr>
					<td> Munka leírása/megnevezése:</td>
					<td>  <textarea class="longinfield" name="Work_Description" rows="4" cols="30" style="text-align-last: center;  overflow:auto; border:1px outset #000000;" ><?php echo $row['Work_Description'];?></textarea>  </td>
				</tr>

				<tr>
					<td> Üzem:</td>
					<td> 
                                            <SELECT name="optionlist"> 
                                                <?php
                                                	$result2 = mysqli_query($con,"SELECT * FROM Departments");
                                                    while ($row2 = mysqli_fetch_array($result2)) 
                                                    {
                                                            $depid = $row2['DepartmentID'];

                                                            $na = $row2['Department_Name'];
                                                            
                                                            if($row['Department_ID'] == $row2['DepartmentID'])
                                                            {
                                                                echo ' <OPTION value=" ' . $depid .' " selected> ' . $na .' </OPTION>\n';
                                                            }
                                                            else
                                                            {
                                                                echo ' <OPTION value=" ' . $depid .' " > ' . $na .' </OPTION>\n';
                                                            }
                                                    } 
                                                ?>
                                            </SELECT>
                                        </td>
				</tr>
				<tr>
					<td align="right"> Módosít ></td>
					<td> <INPUT type="submit" name="kuld" value="OK" ><INPUT type="reset" name="reset" value="Alaphelyzet" >  </td>
				</tr>

			</table>

		</form>

<?php
        }
        //End of IF - Contitional showEditTable DISPLAY

    if($_SERVER['REQUEST_METHOD'] == "POST")
    {

	//connection to MySQL
	require_once('../db_connect.php');

	//GET FIELD VALUES INTO VARs
        $inputdate = $_POST['Date'];
        $inputorder = $_POST['Order_Number'];
        $inputequipmentid = $_POST['Equipment_ID'];
        $textareadescription = $_POST['Work_Description'];
        $dep = $_POST['optionlist'];

	//INSERT DB
        mysqli_query($con, "UPDATE Worklist SET Date='{$inputdate}', Order_Number='{$inputorder}', Equipment_ID='{$inputequipmentid}', Work_Description='{$textareadescription}', Department_ID='{$dep}' WHERE Work_ID='{$_GET['id']}';") ; 
	
        //AFTER UPDATE - PAGE Redirect to worklist.php");
	echo '<script> location.replace("worklist.php"); </script>';
    }


?>

</body>
</html>
