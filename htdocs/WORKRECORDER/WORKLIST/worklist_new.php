<html>

<!-- HTML INNER CSS-->
<style>
	tr:hover {
	    background-color: #ecf3ea;
	}
	table {
			
		    box-shadow: 20px 20px 80px rgba(0,0,0,.7);
	}

	h1 {
			width: 50%;
		    border-bottom-right-radius: 30% 90%;
		    box-shadow: 20px 20px 50px rgba(0,0,0,.7);
	}
	.infield{
			border-top-left-radius: 30% 30%;
		    border-bottom-right-radius: 20% 50%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	.longinfield{
			border-top-left-radius: 10% 40%;
		    border-bottom-right-radius: 10% 40%;
			border-color: #4a8e90;
			background-color: #cfd0d4;
	}
	
</style>

<?php

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

?>
<?php

	//connection to MySQL
	require_once('../db_connect.php');
	$result = mysqli_query($con,"SELECT * FROM Departments");

?>
<head>
<title>Új Munka</title>


</head>
<body>



<?php
        session_start();
        if ($_SESSION['access'] == 0)
        {
            die('<h1> Nem azonosított hozzáférés! </h1> <br/> Jelentkezzen be! <br/>  <a href="../index.php" >Bejelentkezés ></a> ');
        }

/*********************************************
***		MAIN PAGE		   ***
**********************************************  */
	echo '<h1> Új Munka Létrehozása > </h1>';
	$generatedNumber = generateRandomString(25);
	
	// BACK TO WORKERS Button - opens previous Page
	echo '<form action="worklist.php">';
	echo '    <input type="submit" value="<< Vissza" />';
	echo '</form>';


?>
<!-- <button onclick="getQR();">GET</button> -->
<script>
	/*function getQR(){
  		var content = clipboardData.getData('Text');
  		document.forms['newmemberform'].elements['TAX_Number'].value = content;
	}*/

</script>
		<form name="new_work_form" action="worklist_new.php" method="POST">

			<table border="2" bgcolor="a7adc5" align="center">

				<tr>
					<td> Munkavégzés Dátuma:</td>
					<td> <INPUT class="infield" type="date" name="Date" value="<?php echo date("Y-m-d");?>" SIZE="15" ></td>
				</tr>

				<tr>
					<td> Megrendelés azonosító:</td>
					<td> <INPUT class="infield" type="text" name="Order_Number" SIZE="15" ></td>
				</tr>

				<tr>
					<td> Berendezés azonosító:</td>
					<td> <INPUT class="infield" type="text" name="Equipment_ID" SIZE="15" ></td>
				</tr>



				<tr>
					<td> Munka leírása/megnevezése:</td>
					<td>  <textarea class="longinfield" name="Work_Description" rows="4" cols="30" style="text-align-last: center;  overflow:auto; border:1px outset #000000;" >Munka részletei</textarea>  </td>
				</tr>

				<tr>
					<td> Üzem:</td>
					<td> 
                                            <SELECT name="optionlist"> 
                                                <?php 			
                                                    while ($row = mysqli_fetch_array($result)) 
                                                    {
                                                            $depid = $row['DepartmentID'];

                                                            $na = $row['Department_Name'];

                                                            echo ' <OPTION value=" ' . $depid .' " > ' . $na .' </OPTION>\n';

                                                    } 
                                                ?>
                                            </SELECT>
                                        </td>
				</tr>
				<tr>
					<td align="right"> Új Munka ></td>
					<td> <INPUT type="submit" name="kuld" value="OK" ><INPUT type="reset" name="reset" value="Alaphelyzet" >  </td>
				</tr>

			</table>

		</form>

<?php


    if($_SERVER['REQUEST_METHOD'] == "POST")
    {

	//connection to MySQL
	require_once('../db_connect.php');


	//GET FIELD VALUES INTO VARs
        $inputdate = $_POST['Date'];
        $inputorder = $_POST['Order_Number'];
        $inputequipmentid = $_POST['Equipment_ID'];
        $textareadescription = $_POST['Work_Description'];
        $dep = $_POST['optionlist'];

	//INSERT DB
        mysqli_query($con, "INSERT INTO Worklist VALUES ('$generatedNumber','$inputdate','$inputorder','$inputequipmentid','$textareadescription','$dep')") ;
	
        //AFTER UPDATE - PAGE Redirect to workdetails_new.php TO ADD at LEAST 1 Worker !!!!
	echo '<script> location.replace("../WORKDETAILS/workdetails_new.php?work_id='.$generatedNumber.'"); </script>';
    }


?>

</body>
</html>
