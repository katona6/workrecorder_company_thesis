-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Gép: localhost
-- Létrehozás ideje: 2017. Nov 19. 16:31
-- Kiszolgáló verziója: 10.1.25-MariaDB
-- PHP verzió: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `WORKRECORDER`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `Departments`
--

CREATE TABLE `Departments` (
  `DepartmentID` int(5) NOT NULL,
  `Department_Name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `Departments`
--

INSERT INTO `Departments` (`DepartmentID`, `Department_Name`) VALUES
(1, 'Csapágyazó'),
(2, 'DEF'),
(3, 'GHI'),
(7, 'JKL'),
(8, 'Karimahegesztő'),
(9, 'Forgácsoló');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `Workdetails`
--

CREATE TABLE `Workdetails` (
  `WorkDetailID` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `WorkerID` varchar(20) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `WorkID` varchar(30) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Working_Rank` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `MaterialCostOfTask` int(11) DEFAULT NULL,
  `WeekDay_WorkTime_6_18` int(11) DEFAULT NULL,
  `WeekDay_Hourly_Rate_6_18` int(11) DEFAULT NULL,
  `WeekDay_OverWorkTime_18_6` int(11) DEFAULT NULL,
  `WeekDay_OverHourlyRate_18_6` int(11) DEFAULT NULL,
  `WeekEnd_WorkTime_6_18` int(11) DEFAULT NULL,
  `WeekEnd_HourlyRate_6_18` int(11) DEFAULT NULL,
  `WeekEnd_OverWorkTime_18_6` int(11) DEFAULT NULL,
  `WeekEnd_OverHourlyRate_18_6` int(11) DEFAULT NULL,
  `TotalCostOfTask` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Kapcsolótábla(Munkások+Munkák)';

--
-- A tábla adatainak kiíratása `Workdetails`
--

INSERT INTO `Workdetails` (`WorkDetailID`, `WorkerID`, `WorkID`, `Working_Rank`, `MaterialCostOfTask`, `WeekDay_WorkTime_6_18`, `WeekDay_Hourly_Rate_6_18`, `WeekDay_OverWorkTime_18_6`, `WeekDay_OverHourlyRate_18_6`, `WeekEnd_WorkTime_6_18`, `WeekEnd_HourlyRate_6_18`, `WeekEnd_OverWorkTime_18_6`, `WeekEnd_OverHourlyRate_18_6`, `TotalCostOfTask`) VALUES
('A3ppRr1ABMmtCFv7dR3P', 'YpXhpTKuR88nDw8', 'second', 'Targoncás', 1000, 0, 0, 0, 0, 8, 1000, 0, 0, 9000),
('A5ppRr1ABMmtCFv7dR3P', 'eyiQrU3r957uY6d', '4vx371OWKAGXVycx5jJ2kgV32', 'Gépszerelö', 1500, 7, 2000, 2, 2300, 0, 0, 0, 0, 20100),
('A6ppRr1ABMmtCFv7dR3P', 'YpXhpTKuR88nDw8', '4vx371OWKAGXVycx5jJ2kgV32', 'Gépszerelö', 1500, 7, 2000, 2, 2300, 0, 0, 0, 0, 20100),
('ahEldsTQKd4UuSjfDLxgNigCOIB8Vp', 'eyiQrU3r957uY6d', 'WFBIE3nE3pI2nYUqMvesGpuUp', 'Gépszerelö', 0, 2, 1500, 0, 2000, 0, 0, 0, 0, 3000),
('L3ppRr1ABMmtCFv7dR3P', 'eyiQrU3r957uY6d', 'iNVHWQfeKBMjHVoPgmMxCqyH0', 'Gépszerelö', 1000, 12, 2000, 2, 3000, 0, 0, 0, 0, 31000),
('L3ppRr1ABMmtCh4mdR3P', 'byT4Dd0oUfANtBK', 'iNVHWQfeKBMjHVoPgmMxCqyH0', 'Gépszerelö', 1000, 12, 2000, 2, 3000, 0, 0, 0, 0, 31000),
('Z3ppRr1ABMmtCh4mdR3P', '4FDbt1FF2QDOGNg', 'first', 'Ivhegesztö', 500, 12, 1000, 2, 1200, 0, 0, 0, 0, 14900);

--
-- Eseményindítók `Workdetails`
--
DELIMITER $$
CREATE TRIGGER `Workdetails_Insert` BEFORE INSERT ON `Workdetails` FOR EACH ROW BEGIN
        SET new.TotalCostOfTask = new.WeekDay_WorkTime_6_18 * new.WeekDay_Hourly_Rate_6_18 + new.WeekDay_OverWorkTime_18_6 * new.WeekDay_OverHourlyRate_18_6 + new.WeekEnd_WorkTime_6_18 * new.WeekEnd_HourlyRate_6_18 + new.WeekEnd_OverWorkTime_18_6 * new.WeekEnd_OverHourlyRate_18_6 + new.MaterialCostOfTask ;

    END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Workdetails_Update` BEFORE UPDATE ON `Workdetails` FOR EACH ROW BEGIN
        SET new.TotalCostOfTask = new.WeekDay_WorkTime_6_18 * new.WeekDay_Hourly_Rate_6_18 + new.WeekDay_OverWorkTime_18_6 * new.WeekDay_OverHourlyRate_18_6 + new.WeekEnd_WorkTime_6_18 * new.WeekEnd_HourlyRate_6_18 + new.WeekEnd_OverWorkTime_18_6 * new.WeekEnd_OverHourlyRate_18_6 + new.MaterialCostOfTask ;

    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `Worker`
--

CREATE TABLE `Worker` (
  `WorkerID` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  `ID_Card_Number` varchar(10) COLLATE utf8_hungarian_ci NOT NULL,
  `Name` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `TAX_Number` varchar(15) COLLATE utf8_hungarian_ci NOT NULL,
  `TAJ_Number` int(10) NOT NULL,
  `Status` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `HOME_PostCode` int(7) NOT NULL,
  `HOME_City` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `HOME_Street` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `HOME_House` varchar(4) COLLATE utf8_hungarian_ci NOT NULL,
  `HOME_FloorDoor` varchar(10) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Mothers_Name` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `Birth_Place` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `Birth_Date` date NOT NULL,
  `Education` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Telephone_Number` varchar(15) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Email_Address` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Start_Of_JOB` date DEFAULT NULL,
  `End_Of_JOB` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Munkások adatait tároló tábla';

--
-- A tábla adatainak kiíratása `Worker`
--

INSERT INTO `Worker` (`WorkerID`, `ID_Card_Number`, `Name`, `TAX_Number`, `TAJ_Number`, `Status`, `HOME_PostCode`, `HOME_City`, `HOME_Street`, `HOME_House`, `HOME_FloorDoor`, `Mothers_Name`, `Birth_Place`, `Birth_Date`, `Education`, `Telephone_Number`, `Email_Address`, `Start_Of_JOB`, `End_Of_JOB`) VALUES
('4FDbt1FF2QDOGNg', '699740IA', 'Lakatos Géza', '89111111112', 412618911, 'Leszerelt Dolgozó', 3770, 'Sajószentpéter', 'Csizmadia u.', '24', '', 'Csipkés Gizella', 'Nagyatád', '1967-09-25', 'Okleveles Gépszerelö', '0036201234555', 'lakigeza@gmail.com', '2017-09-20', NULL),
('byT4Dd0oUfANtBK', '699750KA', 'Cifradia Kolompár', '89111111113', 412618111, 'Aktiv Dolgozó', 3778, 'Varbó', 'Kulacs u.', '18', NULL, 'Kenyeres Katalin', 'Budapest', '1973-09-11', 'Okleveles épületgépész', '0036301231122', 'cifraseka_koli@yahoo.com', '2017-09-06', NULL),
('eyiQrU3r957uY6d', '199840SA', 'Szedlacsek Sándor', '89112000113', 312412412, 'Aktiv Dolgozó', 3770, 'SAJÓSZENTPÉTER', 'Kodály Zoltán út', '2', '', 'Macskás Mariann', 'Megyaszó', '1957-11-11', 'Kertész', '0036203465660', 'szedlacsek@szedla.com', '2017-10-05', NULL),
('YpXhpTKuR88nDw8', '499620LS', 'Kuzimír Sándor', '89444222245', 412620991, 'Aktiv Dolgozó', 4400, 'Nyíregyháza', 'Vasútállomás u.', '20', '', 'Erélyes Erzsó', 'Tardona', '1981-01-29', 'Rendszergazda', '0036203465661', 'rendszeres@sys.com', '2017-09-30', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `Worklist`
--

CREATE TABLE `Worklist` (
  `Work_ID` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `Date` date NOT NULL,
  `Order_Number` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Equipment_ID` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Work_Description` varchar(500) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Department_ID` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Munkák tábla';

--
-- A tábla adatainak kiíratása `Worklist`
--

INSERT INTO `Worklist` (`Work_ID`, `Date`, `Order_Number`, `Equipment_ID`, `Work_Description`, `Department_ID`) VALUES
('4vx371OWKAGXVycx5jJ2kgV32', '2017-10-09', 'PR0BA002', 'KALAPACS02', 'Második kalapács javítása a Csapágyazó üzemben', 1),
('first', '2017-10-09', 'SM4305522', 'E-4812', 'Légszürö hegesztése', 8),
('iNVHWQfeKBMjHVoPgmMxCqyH0', '2017-10-03', 'PR0BA003', 'KALAPACS03', 'Harmadik kalapács próbája', 3),
('second', '2017-10-08', 'KM3205544', 'UX3832', 'Óriáskompresszor beszerelése', 1),
('WFBIE3nE3pI2nYUqMvesGpuUp', '2017-09-12', 'SEP0912W', 'SEPWork001', 'Sajtos munka', 1);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `Departments`
--
ALTER TABLE `Departments`
  ADD PRIMARY KEY (`DepartmentID`);

--
-- A tábla indexei `Workdetails`
--
ALTER TABLE `Workdetails`
  ADD PRIMARY KEY (`WorkDetailID`),
  ADD KEY `WorkerID` (`WorkerID`),
  ADD KEY `WorkID` (`WorkID`);

--
-- A tábla indexei `Worker`
--
ALTER TABLE `Worker`
  ADD PRIMARY KEY (`WorkerID`);

--
-- A tábla indexei `Worklist`
--
ALTER TABLE `Worklist`
  ADD PRIMARY KEY (`Work_ID`),
  ADD KEY `Department_ID` (`Department_ID`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `Departments`
--
ALTER TABLE `Departments`
  MODIFY `DepartmentID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `Workdetails`
--
ALTER TABLE `Workdetails`
  ADD CONSTRAINT `WorkID` FOREIGN KEY (`WorkID`) REFERENCES `Worklist` (`Work_ID`),
  ADD CONSTRAINT `WorkerID` FOREIGN KEY (`WorkerID`) REFERENCES `Worker` (`WorkerID`);

--
-- Megkötések a táblához `Worklist`
--
ALTER TABLE `Worklist`
  ADD CONSTRAINT `Department_ID` FOREIGN KEY (`Department_ID`) REFERENCES `Departments` (`DepartmentID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
